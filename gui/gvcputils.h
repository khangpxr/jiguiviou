/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVCPUTILS_H
#define GVCPUTILS_H

#include "qtpimpl.hpp"
#include "bootstrapregisters.h"

#include <QObject>

class QNetworkAddressEntry;
class QHostAddress;

namespace Jgv {
namespace Gvcp {
struct DISCOVERY_ACK;
}
}

namespace Jiguiviou {

class GvcpUtilsPrivate;
class GvcpUtils : public QObject
{
    Q_OBJECT

public:
    explicit GvcpUtils(QObject *parent = nullptr);
    virtual ~GvcpUtils() override;

    void setController(const QNetworkAddressEntry &interface);
    QNetworkAddressEntry controller() const;
    QHostAddress controllerAddress() const;

    void setTransmitterAck(const Jgv::Gvcp::DISCOVERY_ACK &ack);
    Jgv::Gvcp::DISCOVERY_ACK transmitterAck() const;
    QHostAddress transmitterAddress() const;

    QHostAddress receiverAddress() const;

    void setGvspIsMulticast(bool multicast);
    bool gvspIsMulticast() const;
    void setGvspMulticastGroup(const QHostAddress &address);
    QHostAddress gvspMulticastGroup() const;

    void listenForBroadcast();
    void stopBroadcastListener();
    void discover(const QHostAddress &peerIP);
    void forceIP(quint64 mac, const QHostAddress &newIP, const QHostAddress &newNetmask, const QHostAddress &newGateway);

    bool monitorTransmitter();
    void releaseTransmitter();
    Jgv::Gvcp::CCPPrivilege transmitterAccessMode();
    QString readXmlFilenameFromDevice();
    void setXmlFilename(const QString &name);
    QString xmlFilename() const;
    QString readXmlFileFromDevice();
    void setXmlFile(const QString &file);
    QString xmlFile() const;
    void setWantControl(bool wanted);
    bool wantControl() const;


signals:
    void newDeviceDiscovered(const Jgv::Gvcp::DISCOVERY_ACK &device);
    void forceIpSucces();

private:
    QT_BASE_PIMPL(GvcpUtils)

};

} // namespace Jiguiviou

#endif // GVCPUTILS_H
