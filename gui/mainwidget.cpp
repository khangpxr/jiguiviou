/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "mainwidget.h"
#include "mainwidget_p.h"

#include "camerasession.h"
#ifdef GSTREAMERCLIENT
#include "gstreamerreceiversession.h"
#endif
#ifdef QTRECEIVER
#include "qtreceiversession.h"
#endif

#ifdef ENDAT
#include <endat.h>
#include "eibdialog.h"
#endif

#include "gvcpdiscoverer.h"
#include "discoveryhelper.h"
#include "networkselectionpage.h"
#include "discoverypage.h"
#include "forceippage.h"
#include "gvsppage.h"
#include "genicamtreeview.h"


#include <QMenu>
#include <QToolBar>
#include <QMenuBar>
#include <QWizard>
#include <QNetworkAddressEntry>
#include <QDockWidget>
#include <QList>
#include <QAction>
#include <QLabel>

namespace Jiguiviou {


EmptyWidget::EmptyWidget(QWidget *parent, Qt::WindowFlags f)
    : QLabel(parent,f)
{
    setAlignment(Qt::AlignCenter);
    setPixmap(QPixmap("://ressources/icons/jiguiviou.png"));
}

} // namespace Jiguiviou

using namespace Jiguiviou;

MainWidget::MainWidget(QWidget *parent)
    : QMainWindow(parent), p_impl(new MainWidgetPrivate)
{
    createWidgets();
    createActions();
    createMenus();
    createToolbar();
}

MainWidget::~MainWidget()
{}

void MainWidget::createWidgets() {
    QT_IMPL(MainWidget);

    setMinimumSize(1024, 768);
    //    setIconSize(iconSize() * 2);
    setWindowIcon(QIcon("://ressources/icons/jiguiviou.png"));

    // le dock de gauche
    d->leftDock = new QDockWidget(trUtf8("Genicam"), this);
    d->leftDock->setAllowedAreas(Qt::LeftDockWidgetArea);
    d->leftDock->setVisible(false);
    addDockWidget(Qt::LeftDockWidgetArea, d->leftDock);
    setCentralWidget(new EmptyWidget);
}

void MainWidget::createActions() {
    QT_IMPL(MainWidget);

    d->startSession = new QAction(QIcon("://ressources/icons/wizard-add.png"),
                                  trUtf8("Créer une session GiGE Vision"), this);
    d->stopSession = new QAction(QIcon("://ressources/icons/remove-camera.png"),
                                 trUtf8("Fermer la session courante"), this);
    d->stopSession->setEnabled(false);
    connect(d->startSession, &QAction::triggered, this, &MainWidget::onStartSession);
    connect(d->stopSession, &QAction::triggered, this, &MainWidget::onStopSession);

    d->toggleLeftDock = d->leftDock->toggleViewAction();
    d->toggleLeftDock->setIcon(QIcon("://ressources/icons/configure-device.png"));
    d->toggleLeftDock->setVisible(false);
    d->toggleLeftDock->setEnabled(false);

}

void MainWidget::createMenus() {
    QT_IMPL(MainWidget);

    // ************************************
    //         Menu gestion de session
    // ************************************
    QMenu *menu = new QMenu(trUtf8("Jiguiviou"), this);
    menu->addAction(d->startSession);
    menu->addAction(d->stopSession);
    menuBar()->addMenu(menu);

#ifdef ENDAT
    // ************************************
    //         Eib Session
    // ************************************
    QMenu *eibMenu = new QMenu(trUtf8("Eib 741"), this);
    eibMenu->setEnabled(true);
    QAction *configureEib = new QAction(trUtf8("Configurer"), this);
    eibMenu->addAction(configureEib);
    menuBar()->addMenu(eibMenu);
    connect(configureEib, &QAction::triggered, [this] () {
        EibDialog eib {this};
        eib.exec();
    });
#endif

    // ************************************
    //         Menu Session
    // ************************************
    d->sessionMenu = new QMenu(trUtf8("Session"), this);
    d->sessionMenu->setEnabled(false);
    menuBar()->addMenu(d->sessionMenu);
}

void MainWidget::createToolbar() {
    QT_IMPL(MainWidget);
    // barre d'outils associée
    d->toolbar = addToolBar(trUtf8("Jiguiviou"));
    d->toolbar->addAction(d->startSession);
    d->toolbar->addAction(d->stopSession);
    d->toolbar->addAction(d->toggleLeftDock);
    d->toolbar->addSeparator();
    d->toolbar->setIconSize(d->toolbar->iconSize() * 2);

}

void MainWidget::onStartSession() {
    QT_IMPL(MainWidget);

#if defined GSTREAMERCLIENT
    QScopedPointer<CameraSession> locale(new GstreamerReceiverSession);
#elif defined QTRECEIVER
    QScopedPointer<CameraSession> locale(new QtReceiverSession);
#else
    QScopedPointer<CameraSession> locale(new CameraSession);
#endif
    if (locale->startSession()) {
        // le widget principal
        setCentralWidget(locale->mainWidget());

        // le dock gauche
        QWidget *widget = locale->leftDockedWidget();
        if (widget != nullptr) {
            d->leftDock->setWidget(widget);
            d->toggleLeftDock->setVisible(true);
            d->toggleLeftDock->setEnabled(true);
        }

        // la barre d'infos
        setStatusBar(locale->statusBar());

        // la toolbar et le menu
        d->sessionMenu->addActions(locale->menuActions());
        d->toolbar->addActions(locale->toolbarActions());
        d->sessionMenu->setEnabled(true);

        d->startSession->setEnabled(false);
        d->stopSession->setEnabled(true);

        // transfert la propriété
        d->session.swap(locale);
    }
}

void MainWidget::onStopSession() {
    QT_IMPL(MainWidget);
    // désactive la vue dock
    d->toggleLeftDock->setEnabled(false);
    d->toggleLeftDock->setVisible(false);
    d->sessionMenu->setEnabled(false);
    d->leftDock->setVisible(false);

    // suppression de la session caméra
    d->session.reset();

    // suppression du widget principal
    setCentralWidget(new EmptyWidget);



    d->startSession->setEnabled(true);
    d->stopSession->setEnabled(false);
}


