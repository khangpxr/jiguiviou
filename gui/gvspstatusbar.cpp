/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspstatusbar.h"
#include "gvspstatusbar_p.h"

#include "gvspdevices.h"

#include <QLabel>
#include <QHostAddress>
#include <QFont>

using namespace Jiguiviou;

GvspStatusBar::GvspStatusBar(QWidget *parent)
    : QStatusBar(parent), p_impl(new GvspStatusBarPrivate)
{}

GvspStatusBar::~GvspStatusBar()
{}

void GvspStatusBar::visualizeReceiver(QSharedPointer<Jgv::Gvsp::Receiver> receiverPtr)
{
    IMPL(GvspStatusBar);
    d->receiverPtr = receiverPtr;

//    QLabel *ips = new QLabel(QString("%0->%1").arg(QHostAddress(receiverPtr->transmitterIP()).toString()).arg(QHostAddress(receiverPtr->receiverIP()).toString()));
//    ips->setFrameStyle(QFrame::Panel | QFrame::Sunken);
//    ips->setToolTip(QObject::trUtf8("Flux GVSP"));
//    ips->setFont(QFont("Monospace"));
//    addPermanentWidget(ips);

    QLabel *completedImages = new QLabel("IC:0");
    completedImages->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    completedImages->setToolTip(QObject::trUtf8("Images Complètes"));
    completedImages->setFont(QFont("Monospace"));
    addPermanentWidget(completedImages);

    QLabel *lostImages = new QLabel("IP:0");
    lostImages->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    lostImages->setToolTip(QObject::trUtf8("Images Perdues"));
    lostImages->setFont(QFont("Monospace"));
    addPermanentWidget(lostImages);

    QLabel *resentSegments = new QLabel("SP:0");
    resentSegments->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    resentSegments->setToolTip(QObject::trUtf8("Segments Redemandés"));
    resentSegments->setFont(QFont("Monospace"));
    addPermanentWidget(resentSegments);

    QLabel *lostSegments = new QLabel("SP:0");
    lostSegments->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    lostSegments->setToolTip(QObject::trUtf8("Segments Perdues"));
    lostSegments->setFont(QFont("Monospace"));
    addPermanentWidget(lostSegments);

    QObject::connect(&d->timer, &QTimer::timeout, [=]() {
        if (d->receiverPtr) {
            completedImages->setText(QString("IC:%0").arg(d->receiverPtr->statistics().imagesCount));
            lostImages->setText(QString("IP:%0").arg(d->receiverPtr->statistics().imagesLostCount));
            resentSegments->setText(QString("SR:%0").arg(d->receiverPtr->statistics().segmentsResendCount));
            lostSegments->setText(QString("SP:%0").arg(d->receiverPtr->statistics().segmentsLostCount));
        }
    });

    d->timer.start(500);
}
