/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "camerasession.h"
#include "camerasession_p.h"

#include "gvcputils.h"
#include "networkselectionpage.h"
#include "discoverypage.h"
#include "forceippage.h"
#include "cameravalidationpage.h"
#include "gvsppage.h"
#include "gvspwidget.h"
#include "endatwidget.h"

#ifdef EXTERNSYNC
#include "syncexternwidget.h"
#endif

#include "genicammodel.h"
#include "genicamtreeview.h"
#include <geviport.h>
#include <gvspdevices.h>
#include <genicam.h>

#ifdef ENDAT
#include <endat.h>
#include "eibdialog.h"
#endif

#include <QAction>
#include <QHostAddress>
#include <QVBoxLayout>
#include <QDialog>
#include <QWizard>
#include <QDialogButtonBox>

using namespace Jiguiviou;

WidgetDialog::WidgetDialog(QWidget *widget, const QString &windowTitle)
{
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(widget);

    setWindowTitle(windowTitle);
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    layout->addWidget(buttonBox);

    setLayout(layout);
}

CameraSessionPrivate::~CameraSessionPrivate()
{}

void CameraSessionPrivate::initControllerReceiverSession(QSharedPointer<GvcpUtils> gvcpUtilsPtr, QObject *parent)
{
    const QHostAddress controller { gvcpUtilsPtr->controllerAddress() };
    const QHostAddress transmitter { gvcpUtilsPtr->transmitterAddress() };
    const QHostAddress receiver { gvcpUtilsPtr->receiverAddress() };
    const QHostAddress multicast { gvcpUtilsPtr->gvspMulticastGroup() };

    // on crée le controleur
    QSharedPointer<Jgv::GevIPort> iport(new Jgv::GevIPort);
    if (!iport->controleDevice(controller.toIPv4Address(), transmitter.toIPv4Address())) {
        qWarning("CameraSession failed to control device");
    }

    // création du modèle sur le fichier du device, avec notre controleur
    Jgv::GenICam::Model *deviceModel = new Jgv::GenICam::Model(iport, parent);
    deviceModel->setGenICamXml(gvcpUtilsPtr->xmlFilename(), gvcpUtilsPtr->xmlFile().toUtf8());

    // les actions de contrôle de flux sur le device model
    QAction *action = new QAction(QIcon("://ressources/icons/run.png"), QObject::trUtf8("Démarrer la diffusion"), parent);
    QObject::connect(action, &QAction::triggered, [deviceModel]() {
        deviceModel->setValue("AcquisitionStart", QVariant());
    });
    toolbarActions.append(action);
    menuActions.append(action);
    action = new QAction(QIcon("://ressources/icons/stop.png"), QObject::trUtf8("Stopper la diffusion"), parent);
    QObject::connect(action, &QAction::triggered, [deviceModel]() {
        deviceModel->setValue("AcquisitionStop", QVariant());
    });
    toolbarActions.append(action);
    menuActions.append(action);

    // le treeview sur le modèle device sera docké à gauche
    GenICamTreeView *treeview = new GenICamTreeView;
    treeview->setModel(deviceModel);
    // transfert la propriété du widget au scoped pointeur
    leftWidgetPtr.reset(treeview);

    createReceiver(parent);

    // met le receveur en écoute
    if (gvcpUtilsPtr->gvspIsMulticast()) {
        receiverPtr->listenMulticast(receiver.toIPv4Address(), multicast.toIPv4Address());
        // informe l'IPort des coordonnées du receveur
        iport->addReceiver(0, multicast.toIPv4Address(), receiverPtr->parameters().receiverPort);
    }
    else {
        receiverPtr->listenUnicast(receiver.toIPv4Address());
        iport->addReceiver(0, receiver.toIPv4Address(), receiverPtr->parameters().receiverPort);
    }



    // modèle générique sur le bootstrap
    Jgv::GenICam::Model bootstrapModel(iport);
    bootstrapModel.setGenICamXml("bootstrap.xml", Jgv::GenICam::BootstrapXml::file());
    bootstrapModel.setValue("GevStreamChannelSelector", 0); // selectionne la sortie 0

    // filtre
    receiverPtr->acceptFrom(transmitter.toIPv4Address(), static_cast<uint16_t>(bootstrapModel.getValue("GevSCSP").toUInt()));

    // préallocation des images
    Jgv::Gvsp::Geometry geometry;
    geometry.width = deviceModel->getValue("Width").toUInt();
    geometry.height = deviceModel->getValue("Height").toUInt();
    geometry.pixelFormat = deviceModel->getValue("PixelFormat").toUInt();
    uint32_t packetSize = bootstrapModel.getValue("GevSCPSPacketSize").toUInt();
    receiverPtr->preallocImages(geometry, packetSize);

    // fournit la fréquence horloge
    receiverPtr->setTransmitterTimestampFrequency(bootstrapModel.getValue("TimestampTickFrequency").toULongLong());
    // active le système de datation
    auto date = [this, iport] () {
        const Jgv::Gvcp::Datation datation = iport->getDatation();
        if (datation.timestamp != 0) {
            receiverPtr->pushDatation(datation.timestamp, datation.dateMin, datation.dateMax);
        }
    };
    QObject::connect(&dateTimer, &QTimer::timeout, date);
    dateTimer.start(10000);


    // on joute una action pour éditer le bootstrap
    action = new QAction(QObject::trUtf8("Bootstrap Editor"), parent);
    QObject::connect(action, &QAction::triggered, [iport]() {
        Jgv::GenICam::Model *bootstrapModel = new Jgv::GenICam::Model(iport);
        bootstrapModel->setGenICamXml("bootstrap.xml", Jgv::GenICam::BootstrapXml::file());
        GenICamTreeView *treeView = new GenICamTreeView;
        treeView->setModel(bootstrapModel, false);
        WidgetDialog dialog(treeView, "Bootstrap Editor");
        QObject::connect(&dialog, &QDialog::destroyed, bootstrapModel, &QAbstractItemModel::deleteLater);
        dialog.setMinimumSize(dialog.sizeHint() * 2);
        dialog.exec();
    });
    menuActions.append(action);

    // initialise le widget de statistics
    statusBarPtr.reset(new GvspStatusBar);
    statusBarPtr->visualizeReceiver(receiverPtr);

#ifdef ENDAT
    auto endatPtr = receiverPtr->getEndat().lock();
    EndatWidget::restoreSettings(receiverPtr->getEndat());

    endatPtr->startSoftRealTime();




    action = new QAction(QObject::trUtf8("Endat"), parent);
    QObject::connect(action, &QAction::triggered, [this]() {
        EndatWidget *widget = new EndatWidget(receiverPtr->getEndat());
        WidgetDialog dialog(widget, "Configuration Eib 741");
        dialog.setMinimumSize(dialog.sizeHint() * 2);
        if (dialog.exec() == QDialog::Accepted) {
            EndatWidget::saveSettings(receiverPtr->getEndat());
        } else {
            EndatWidget::restoreSettings(receiverPtr->getEndat());
        }
    });
    menuActions.append(action);
#endif

    // lance la diffusion
    deviceModel->setValue("AcquisitionStart", QVariant());

#ifdef EXTERNSYNC
    // gestion du synchronisateur externe
    action = new QAction(QObject::trUtf8("Synchro externe"), parent);
    QObject::connect(action, &QAction::triggered, []() {
        SyncExternWidget *widget = new SyncExternWidget;
        WidgetDialog dialog(widget, "Configuration synchro externe");
        dialog.setMinimumSize(dialog.sizeHint() * 2);
        if (dialog.exec() == QDialog::Accepted) {
            //EndatWidget::saveSettings(receiverPtr->getEndat());
        } else {
            //EndatWidget::restoreSettings(receiverPtr->getEndat());
        }
    });
    menuActions.append(action);
#endif

}

void CameraSessionPrivate::initSimpleMonitorSession(QSharedPointer<GvcpUtils> gvcpUtilsPtr, QObject *parent)
{
    QHostAddress controller = gvcpUtilsPtr->controllerAddress();
    QHostAddress transmitter = gvcpUtilsPtr->transmitterAddress();

    // on crée le controleur
    QSharedPointer<Jgv::GevIPort> iport(new Jgv::GevIPort);
    iport->monitorDevice(controller.toIPv4Address(), transmitter.toIPv4Address());

    // création du modèle MVC, avec notre controleur
    Jgv::GenICam::Model *model = new Jgv::GenICam::Model(iport, parent);
    model->setGenICamXml(gvcpUtilsPtr->xmlFilename(), gvcpUtilsPtr->xmlFile().toUtf8());

    // le treeview sera le widget central
    GenICamTreeView *treeview = nullptr;
    mainWidgetPtr.reset(treeview = new GenICamTreeView);
    treeview->setModel(model);

    QAction *bootstrapAction = new QAction(QObject::trUtf8("Bootstrap"), parent);
    QObject::connect(bootstrapAction, &QAction::triggered, [iport]() {
        Jgv::GenICam::Model *bootstrapModel = new Jgv::GenICam::Model(iport);
        bootstrapModel->setGenICamXml("bootstrap.xml", Jgv::GenICam::BootstrapXml::file());
        GenICamTreeView *treeView = new GenICamTreeView;
        treeView->setModel(bootstrapModel, false);
        WidgetDialog dialog(treeView, "Bootstrap Editor");
        QObject::connect(&dialog, &QDialog::destroyed, bootstrapModel, &QAbstractItemModel::deleteLater);
        dialog.setMinimumSize(dialog.sizeHint() * 2);
        dialog.exec();
    });
    menuActions.append(bootstrapAction);

}

void CameraSessionPrivate::createReceiver(QObject *parent)
{
    receiverPtr = QSharedPointer<Jgv::Gvsp::Receiver>::create();
    //receiverPtr.reset(new Jgv::Gvsp::Receiver);
    GvspWidget *allocatorWidget = new GvspWidget;
    allocatorWidget->visualizeReceiver(receiverPtr);
    mainWidgetPtr.reset(allocatorWidget);

    // ajoute une action pour gérer les paramètres du receveur
    QAction *action = new QAction(QObject::trUtf8("Réglages receveur"), parent);
    QObject::connect(action, &QAction::triggered, [=]() {
        GvspWidget *w = new GvspWidget;
        w->visualizeReceiver(receiverPtr, 200);
        WidgetDialog dialog(w, QObject::trUtf8("Receveur"));
        dialog.exec();
    });
    menuActions.append(action);

}

CameraSession::CameraSession(QObject *parent)
    : QObject(parent), p_impl(new CameraSessionPrivate)
{}

CameraSession::CameraSession(CameraSessionPrivate &dd, QObject *parent)
    : QObject(parent), p_impl(&dd)
{}

CameraSession::~CameraSession()
{}


QWidget *CameraSession::mainWidget() const
{
    IMPL(const CameraSession);
    return d->mainWidgetPtr.data();
}

QWidget *CameraSession::leftDockedWidget() const
{
    IMPL(const CameraSession);
    return d->leftWidgetPtr.data();
}

QStatusBar *CameraSession::statusBar() const
{
    IMPL(const CameraSession);
    return d->statusBarPtr.data();
}

QWidget *CameraSession::bootstrapWidget() const
{
    IMPL(const CameraSession);
    return d->bootstrapPtr.data();
}

QList<QAction *> CameraSession::menuActions() const
{
    IMPL(const CameraSession);
    return d->menuActions;
}

QList<QAction *> CameraSession::toolbarActions() const
{
    IMPL(const CameraSession);
    return d->toolbarActions;
}

bool CameraSession::startSession()
{
    IMPL(CameraSession);

    QSharedPointer<GvcpUtils> gvcpUtils(new GvcpUtils);
    QWizard wizard;
    wizard.setWindowTitle(trUtf8("Configurer pour une caméra GiGE Vision"));
    wizard.addPage(new NetworkSelectionPage(gvcpUtils));
    wizard.addPage(new DiscoveryPage(gvcpUtils));
    wizard.addPage(new ForceIPPage(gvcpUtils));
    wizard.addPage(new CameraValidationPage(gvcpUtils));
    wizard.addPage(new GvspPage(gvcpUtils));

    if (wizard.exec() == QDialog::Accepted) {
        if (gvcpUtils->wantControl()) {
            d->initControllerReceiverSession(gvcpUtils, this);
        }
        else {
            d->initSimpleMonitorSession(gvcpUtils, this);
        }
        return true;
    }

    return false;
}

