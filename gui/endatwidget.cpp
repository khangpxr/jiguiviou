/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "endatwidget.h"

#include <endat.h>
#include "htmltable.h"

#include <limits>

#include <QFormLayout>
#include <QTabWidget>
#include <QLineEdit>
#include <QHostAddress>
#include <QCheckBox>
#include <QSpinBox>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QVariant>

using namespace Jiguiviou;

namespace {

QString statusToString(ConnectionStatus status) {
    switch (status) {
    case ConnectionStatus::Closed:
        return "Non connecté";
    case ConnectionStatus::Connected:
        return "Connecté";
    case ConnectionStatus::Reset:
        return "Reset par Eib";
    case ConnectionStatus::Timeout:
        return "Timeout";
    case ConnectionStatus::TramsmissionError:
        return "Erreur transmission";
    case ConnectionStatus::libeibError:
        return "Erreur libeib";
    }
    return "Unknow";
}

} // Anonymous namespace


EndatWidget::EndatWidget(std::weak_ptr<Endat> endatPtr, QWidget *parent)
    : QWidget(parent)
{
    auto endat = endatPtr.lock();


    QFormLayout *layout = new QFormLayout;
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);

    QLineEdit *ip = new QLineEdit;
    ip->setAlignment(Qt::AlignCenter);
    ip->setReadOnly(true);
    ip->setText(QHostAddress(endat->eibAddress()).toString());
    layout->addRow(trUtf8("Adresse Eib 741"), ip);

    QLineEdit *ipStatus = new QLineEdit;
    ipStatus->setAlignment(Qt::AlignCenter);
    ipStatus->setReadOnly(true);
    ipStatus->setText(statusToString(endat->connectionStatus()));
    layout->addRow(trUtf8("Status de la connexion"), ipStatus);

    QComboBox *units = new QComboBox;
    units->addItem(trUtf8("MilliGrades"), 400000);
    units->addItem(trUtf8("MilliDegrés"), 360000);
    layout->addRow(trUtf8("Unité d'angle"), units);

    QTabWidget *tabs = new QTabWidget;
    layout->addRow(tabs);

    if (endat->axeSettings(1).dynamic == 360000) {
        units->setCurrentIndex(1);
    }

    {
        QFormLayout *layout = new QFormLayout;

        AxesReference reference = endat->userAxesReference();

        QSpinBox *siteRef = new QSpinBox;
        siteRef->setRange(-999999, 999999);
        siteRef->setValue(static_cast<int>(reference.firstAxe));
        layout->addRow(trUtf8("Réference site"), siteRef);


        QSpinBox *gisementRef = new QSpinBox;
        gisementRef->setRange(-999999, 999999);
        gisementRef->setValue(static_cast<int>(reference.secondAxe));
        layout->addRow(trUtf8("Réference gisement"), gisementRef);

        QPushButton *apply = new QPushButton(trUtf8("Appliquer"));
        layout->addRow("Appliquer les changements", apply);

        connect(apply, &QPushButton::clicked, [endatPtr, siteRef, gisementRef] () {
            auto endat = endatPtr.lock();
            endat->setUserAxesReference(AxesReference {siteRef->value(), gisementRef->value()} );
        });


        QFrame * tab = new QFrame;
        tab->setLayout(layout);
        tabs->addTab(tab, trUtf8("Calibration codeurs"));
    }

    {
        QFormLayout *layout = new QFormLayout;

        auto settings = endat->axeSettings(1);

        QCheckBox *inversion = new QCheckBox;
        layout->addRow(trUtf8("Inverser les sens de rotation"), inversion);
        inversion->setChecked(settings.inverted);

        connect(inversion, &QCheckBox::stateChanged, [endatPtr] (int value) {
            auto endat = endatPtr.lock();
            auto settings = endat->axeSettings(1);
            settings.inverted = (value == Qt::Checked);
            endat->setAxeSettings(1, settings);
        });

        QSpinBox *origin = new QSpinBox;
        origin->setRange(-999999, 999999);
        origin->setValue(static_cast<int>(settings.origin));
        layout->addRow(trUtf8("Origine de l'angle"), origin);

        connect(origin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [endatPtr] (int value) {
            auto endat = endatPtr.lock();
            auto settings = endat->axeSettings(1);
            settings.origin = value;
            endat->setAxeSettings(1, settings);
        });

        QSpinBox *dynamic = new QSpinBox;
        dynamic->setReadOnly(true);
        dynamic->setRange(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
        dynamic->setValue(static_cast<int>(settings.dynamic));
        layout->addRow(trUtf8("Nombre de points"), dynamic);

        connect(units, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),  [dynamic, units] () {
            dynamic->setValue(units->currentData().toInt());
        });

        QFrame * tab = new QFrame;
        tab->setLayout(layout);
        tabs->addTab(tab, trUtf8("Axe site"));
    }

    {
        QFormLayout *layout = new QFormLayout;

        auto settings = endat->axeSettings(2);

        QCheckBox *inversion = new QCheckBox;
        layout->addRow(trUtf8("Inverser les sens de rotation"), inversion);
        inversion->setChecked(settings.inverted);

        connect(inversion, &QCheckBox::stateChanged, [endatPtr] (int value) {
            auto endat = endatPtr.lock();
            auto settings = endat->axeSettings(2);
            settings.inverted = (value == Qt::Checked);
            endat->setAxeSettings(2, settings);
        });

        QSpinBox *origin = new QSpinBox;
        origin->setRange(-999999, 999999);
        origin->setValue(static_cast<int>(settings.origin));
        layout->addRow(trUtf8("Origine de l'angle"), origin);

        connect(origin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [endatPtr] (int value) {
            auto endat = endatPtr.lock();
            auto settings = endat->axeSettings(2);
            settings.origin = value;
            endat->setAxeSettings(2, settings);
        });

        QSpinBox *dynamic = new QSpinBox;
        dynamic->setReadOnly(true);
        dynamic->setRange(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
        dynamic->setValue(static_cast<int>(settings.dynamic));
        layout->addRow(trUtf8("Nombre de points"), dynamic);

        connect(units, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),  [dynamic, units] () {
            dynamic->setValue(units->currentData().toInt());
        });

        QFrame * tab = new QFrame;
        tab->setLayout(layout);
        tabs->addTab(tab, trUtf8("Axe gisement"));

    }

    { // Eib741 tab
        QFormLayout *layout = new QFormLayout;
        layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);

        auto macToString = [] (uint64_t address) {
            return QString("%0:%1:%2:%3:%4:%5")
                    .arg(0xFF & (address >> 0x28), 2, 16, QLatin1Char('0'))
                    .arg(0xFF & (address >> 0x20), 2, 16, QLatin1Char('0'))
                    .arg(0xFF & (address >> 0x18), 2, 16, QLatin1Char('0'))
                    .arg(0xFF & (address >> 0x10), 2, 16, QLatin1Char('0'))
                    .arg(0xFF & (address >> 0x8), 2, 16, QLatin1Char('0'))
                    .arg(0xFF & address, 2, 16, QLatin1Char('0'))
                    .toUpper();
        };

        QLabel *infos = new QLabel;
        infos->setStyleSheet("background-color: white");
        EibInfos eibInfos = endat->eibInfos();
        HtmlTable table;
        table.addRow(trUtf8("Eib ID"), QString::fromStdString(eibInfos.id));
        table.addRow(trUtf8("Hostname"), QString::fromStdString(eibInfos.hostname));
        table.addRow(trUtf8("IP"), QHostAddress(eibInfos.network.ip).toString());
        table.addRow(trUtf8("Netmask"), QHostAddress(eibInfos.network.netmask).toString());
        table.addRow(trUtf8("Gateway"), QHostAddress(eibInfos.network.gateway).toString());
        table.addRow(trUtf8("Adresse MAC"), macToString(eibInfos.mac));
        infos->setText(table);
        layout->addRow(trUtf8("Infos Eib"), infos);

        QFrame * tab = new QFrame;
        tab->setLayout(layout);
        tabs->addTab(tab, trUtf8("Eib 741"));

    }

    setLayout(layout);
}

void EndatWidget::saveSettings(std::weak_ptr<Endat> endatPtr)
{
    auto endat = endatPtr.lock();
    QSettings settings;

    settings.beginGroup("Endat");

    settings.setValue("Initialized", true);

    settings.beginWriteArray("Axe");
    for (int i=0; i<2; ++i) {
        AxeSettings axes = endat->axeSettings(i + 1);
        settings.setArrayIndex(i);
        settings.setValue("inverted", QVariant(axes.inverted));
        settings.setValue("origin", QVariant(static_cast<qint64>(axes.origin)));
        settings.setValue("dynamic", QVariant(static_cast<qint64>(axes.dynamic)));
    }
    settings.endArray();

    settings.beginWriteArray("Reference");

    AxesReference reference = endat->axesReference();
    AxesReference userReference = endat->userAxesReference();
    settings.setArrayIndex(0);
    settings.setValue("encoder", QVariant(static_cast<qint64>(reference.firstAxe)));
    settings.setValue("user", QVariant(static_cast<qint64>(userReference.firstAxe)));
    settings.setArrayIndex(1);
    settings.setValue("encoder", QVariant(static_cast<qint64>(reference.secondAxe)));
    settings.setValue("user", QVariant(static_cast<qint64>(userReference.secondAxe)));

    settings.endArray();

    settings.endGroup();

    settings.sync();
}

void EndatWidget::restoreSettings(std::weak_ptr<Endat> endatPtr)
{
    auto endat = endatPtr.lock();
    QSettings settings;
    settings.beginGroup("Eib741");
    QHostAddress ip {settings.value("Address", QHostAddress(endat->eibAddress()).toString()).toString()};
    endat->setEibAddress(ip.toIPv4Address());
    settings.endGroup();


    settings.beginGroup("Endat");
    if (!settings.value("Initialized", false).toBool()) {
        return;
    }

    settings.beginReadArray("Axe");
    for (int i=0; i<2; ++i) {
        settings.setArrayIndex(i);
        endat->setAxeSettings(i + 1, AxeSettings {settings.value("inverted").toBool(),
                                                  settings.value("origin").toLongLong(),
                                                  settings.value("dynamic").toLongLong()});

    }
    settings.endArray();

    settings.beginReadArray("Reference");
    AxesReference reference { 0, 0};
    AxesReference userReference { 0, 0};
    settings.setArrayIndex(0);
    reference.firstAxe = settings.value("encoder").toLongLong();
    userReference.firstAxe = settings.value("user").toLongLong();
    settings.setArrayIndex(1);
    reference.secondAxe = settings.value("encoder").toLongLong();
    userReference.secondAxe = settings.value("user").toLongLong();
    endat->restoreAxesReference(userReference, reference);
    settings.endArray();

    settings.endGroup();
}
