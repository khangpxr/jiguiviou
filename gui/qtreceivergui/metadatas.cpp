/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "metadatas.h"
#include "gvspqtreceiver.h"
#include "gvspqtreceiverconfig.h"

#include <QLineEdit>
#include <QFormLayout>

using namespace Jiguiviou;

Jiguiviou::Metadatas::Metadatas(QWidget *parent)
    : QWidget(parent)
{}

void Metadatas::visualizeReceiver(QSharedPointer<Jgv::Gvsp::Receiver> receiverPtr)
{
    auto qtReceiverPtr = receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>();
    if (!qtReceiverPtr) {
        qWarning("Timecode widget failed to cast receiver");
        return;
    }

    auto getConfigPtr = [qtReceiverPtr]() {
        return qtReceiverPtr->configPtr().toStrongRef();
    };

    auto *triedre = new QLineEdit;
    auto *lyre = new QLineEdit;
    auto *focale = new QLineEdit;
    auto *camera = new QLineEdit;
    auto *campagne = new QLineEdit;
    auto *cri = new QLineEdit;
    auto *angleUnit = new QLineEdit;

    auto config = getConfigPtr();
    if (config) {
        triedre->setText(config->jpegXmpMetadata("Triedre"));
        lyre->setText(config->jpegXmpMetadata("Lyre"));
        focale->setText(config->jpegXmpMetadata("Focale"));
        camera->setText(config->jpegXmpMetadata("Camera"));
        campagne->setText(config->jpegXmpMetadata("Campagne"));
        cri->setText(config->jpegXmpMetadata("CRI"));
        angleUnit->setText(config->jpegXmpMetadata("UniteAngle"));
    }

    auto *layout = new QFormLayout;
    layout->addRow(trUtf8("Trièdre"), triedre);
    layout->addRow(trUtf8("Lyre"), lyre);
    layout->addRow(trUtf8("Focale"), focale);
    layout->addRow(trUtf8("Camera"), camera);
    layout->addRow(trUtf8("Campagne"), campagne);
    layout->addRow(trUtf8("CRI"), cri);
    layout->addRow(trUtf8("Unité angle"), angleUnit);
    setLayout(layout);

    connect(triedre, &QLineEdit::editingFinished, [getConfigPtr, triedre] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("Triedre", triedre->text());
        }
    });
    connect(lyre, &QLineEdit::editingFinished, [getConfigPtr, lyre] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("Lyre", lyre->text());
        }
    });
    connect(focale, &QLineEdit::editingFinished, [getConfigPtr, focale] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("Focale", focale->text());
        }
    });
    connect(camera, &QLineEdit::editingFinished, [getConfigPtr, camera] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("Camera", camera->text());
        }
    });
    connect(campagne, &QLineEdit::editingFinished, [getConfigPtr, campagne] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("Campagne", campagne->text());
        }
    });
    connect(cri, &QLineEdit::editingFinished, [getConfigPtr, cri] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("CRI", cri->text());
        }
    });
    connect(angleUnit, &QLineEdit::editingFinished, [getConfigPtr, angleUnit] () {
        auto config = getConfigPtr();
        if (config) {
            getConfigPtr()->setJpegXmpMetadata("UniteAngle", angleUnit->text());
        }
    });
}
