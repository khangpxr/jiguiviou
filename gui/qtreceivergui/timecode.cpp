/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "timecode.h"
#include "gvspqtreceiver.h"
#include "gvspqtreceiverconfig.h"

#include <QCheckBox>
#include <QGroupBox>
#include <QSpinBox>
#include <QComboBox>
#include <QVBoxLayout>
#include <QFormLayout>

using namespace Jiguiviou;

Jiguiviou::Timecode::Timecode(QWidget *parent)
    : QWidget(parent)
{}

void Timecode::visualizeReceiver(QSharedPointer<Jgv::Gvsp::Receiver> receiverPtr)
{
    auto qtReceiverPtr = receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>();
    if (!qtReceiverPtr) {
        qWarning("Timecode widget failed to cast receiver");
        return;
    }

    auto getConfigPtr = [qtReceiverPtr]() {
        return qtReceiverPtr->configPtr().toStrongRef();
    };

    auto config = getConfigPtr();

    auto *overlayGroupBox = new QGroupBox(trUtf8("Overlay"));
    {
        overlayGroupBox->setCheckable(true);
        overlayGroupBox->setChecked(false);
        
        auto *fontSize = new QSpinBox;
        fontSize->setSuffix(" %");
        fontSize->setRange(1, 100);

        auto *alignment = new QComboBox;
        alignment->addItem(trUtf8("Haut Gauche"), static_cast<int>(Jgv::Gvsp::QtBackend::TimecodeAlignment::TopLeft));
        alignment->addItem(trUtf8("Haut Centre"), static_cast<int>(Jgv::Gvsp::QtBackend::TimecodeAlignment::TopCenter));
        alignment->addItem(trUtf8("Haut Droit"), static_cast<int>(Jgv::Gvsp::QtBackend::TimecodeAlignment::TopRight));
        alignment->addItem(trUtf8("Bas Gauche"), static_cast<int>(Jgv::Gvsp::QtBackend::TimecodeAlignment::BottomLeft));
        alignment->addItem(trUtf8("Bas Centre"), static_cast<int>(Jgv::Gvsp::QtBackend::TimecodeAlignment::BottomCenter));
        alignment->addItem(trUtf8("Bas Droit"), static_cast<int>(Jgv::Gvsp::QtBackend::TimecodeAlignment::BottomRight));

        if (config) {
            overlayGroupBox->setChecked(config->overlayTimecodeIsActive());
            fontSize->setValue(config->overlayTimecodeFontSize());
            int i = alignment->findData(static_cast<int>(config->overlayTimecodeAlignment()));
            alignment->setCurrentIndex(i);
        }

        auto *layout = new QFormLayout;
        layout->addRow(trUtf8("Taille de la police"), fontSize);
        layout->addRow(trUtf8("Allignement du timecode"), alignment);
        overlayGroupBox->setLayout(layout);


        connect(overlayGroupBox, &QGroupBox::toggled, [getConfigPtr] (bool checked) {
            auto config = getConfigPtr();
            if (config) {
                config->setOverlayTimecodeActive(checked);
            }
        });
        connect(fontSize, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [getConfigPtr](int value) {
            auto config = getConfigPtr();
            if (config) {
                config->setOverlayTimecodeFontHeight(value);
            }
        });
        connect(alignment, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [alignment, getConfigPtr] (int index) {
            auto config = getConfigPtr();
            if (config) {
                config->setOverlayTimecodeAlignment(static_cast<Jgv::Gvsp::QtBackend::TimecodeAlignment>(alignment->itemData(index).toInt()));
            }
        });


    }

    auto *layout = new QVBoxLayout;
    layout->addWidget(overlayGroupBox);
    layout->addStretch();

    setLayout(layout);
}
