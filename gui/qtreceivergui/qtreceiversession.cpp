/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "qtreceiversession.h"
#include "qtreceiversession_p.h"

#include "gvspqtreceiver.h"
#include "gvspqtreceiverconfig.h"
#include "../gvspwidget.h"
#include "timecode.h"
#include "jpegrecorder.h"
#ifdef XMP
#include "metadatas.h"
#endif
#include "contrast.h"
#include "glvideowindow.h"

#include <QAction>
#include <QStackedWidget>
#include <QWindow>
#include <QOpenGLContext>

#define WIDGET_STACK qobject_cast<QStackedWidget *>(mainWidgetPtr.data())

using namespace Jiguiviou;

void QtReceiverSessionPrivate::createReceiver(QObject *parent)
{
    receiverPtr = QSharedPointer<Jgv::Gvsp::QtBackend::Receiver>::create();
    if (!receiverPtr) {
        return;
    }

    auto getConfigPtr = [this]() {
        auto p = receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>();
        return p->configPtr().toStrongRef();
    };

    auto window = new Jgv::GLVideoWindow(QOpenGLContext::globalShareContext());
    window->setReceiverConfig(receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>()->configPtr());
    auto video = QWidget::createWindowContainer(window);


    receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>()->setGLVideoRenderer(window->wrapper());
    mainWidgetPtr.reset(video);
    receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>()->setSharedContext(window->shareContext());

    QObject::connect(window, &Jgv::GLVideoWindow::changeWindowState, [video]() {
        if (video->isWindow()) {
            video->setWindowFlag(Qt::Window, false);
            video->showMaximized();
            video->unsetCursor();

        }
        else {
            video->setWindowFlag(Qt::Window);
            video->showFullScreen();
            video->setCursor(Qt::BlankCursor);
        }
    });

    QAction *toggleGvsp = new QAction(QObject::trUtf8("Réglages receveur"), parent);
    QObject::connect(toggleGvsp, &QAction::triggered,[this]() {
        GvspWidget *w = new GvspWidget;
        w->visualizeReceiver(receiverPtr);
        WidgetDialog dialog(w, QObject::trUtf8("Réglages receveur"));
        dialog.exec();

    });

    QAction *toggleRecord = new QAction(QIcon("://ressources/icons/record-off.png"), QObject::trUtf8("Enregistrement"), parent);
    toggleRecord->setCheckable(true);
    QObject::connect(toggleRecord, &QAction::triggered, [getConfigPtr] (bool checked) {
        auto config = getConfigPtr();
        if (config) {
            config->setJpegIsRecording(checked);
        }
    });

    QAction *toggleTimecodeSettings = new QAction(QObject::trUtf8("Réglage timecode"), parent);
    QObject::connect(toggleTimecodeSettings, &QAction::triggered, [this] () {
        Timecode *timecode = new Timecode;
        timecode->visualizeReceiver(receiverPtr);
        WidgetDialog dialog(timecode, QObject::trUtf8("Réglage timecode"));
        dialog.exec();
    });

    QAction *toggleJpegRecorderSettings = new QAction(QObject::trUtf8("Réglage enregistreur jpeg"), parent);
    QObject::connect(toggleJpegRecorderSettings, &QAction::triggered, [this] () {
        JpegRecorder *recorder = new JpegRecorder;
        recorder->visualizeReceiver(receiverPtr);
        WidgetDialog dialog(recorder, QObject::trUtf8("Réglage enregistreur Jpeg"));
        dialog.exec();
    });

#ifdef XMP
    QAction *toggleMetadatasSettings = new QAction(QObject::trUtf8("Gestion des métadatas"), parent);
    QObject::connect(toggleMetadatasSettings, &QAction::triggered, [this] () {
        Metadatas *metas = new Metadatas;
        metas->visualizeReceiver(receiverPtr);
        WidgetDialog dialog(metas, QObject::trUtf8("Gestion des métadatas"));
        dialog.exec();
    });
#endif
    QAction *toggleContrastSettings = new QAction(QObject::trUtf8("Amélioration du contraste"), parent);
    QObject::connect(toggleContrastSettings, &QAction::triggered, [this, getConfigPtr] () {
        auto config = getConfigPtr();
        if (config) {
            config->setNonHistogramZoneIsMasked(true);
        }
        Contrast *contrast = new Contrast;
        contrast->visualizeReceiver(receiverPtr);
        WidgetDialog dialog(contrast, QObject::trUtf8("Égalisation du contraste"));
        dialog.exec();
        if (config) {
            config->setNonHistogramZoneIsMasked(false);
        }
    });

    QAction *toggleTimecode = new QAction(QIcon("://ressources/icons/timecode.png"), QObject::trUtf8("Afficher le timecode"), parent);
    QObject::connect(toggleTimecode, &QAction::triggered, [getConfigPtr] () {
        auto config = getConfigPtr();
        if (config) {
            config->setOverlayTimecodeActive(!config->overlayTimecodeIsActive());
        }
    });

    QAction *toggleReticle = new QAction(QIcon("://ressources/icons/reticle.png"), QObject::trUtf8("Afficher le réticule"), parent);
    QObject::connect(toggleReticle, &QAction::triggered, [getConfigPtr] () {
        auto config = getConfigPtr();
        if (config) {
            config->setOverlayReticuleIsActive(!config->overlayReticuleIsActive());
        }
    });

    QAction *toggleHistogramView = new QAction(QIcon("://ressources/icons/histogram.png"), QObject::trUtf8("Visualiser les histogrammes"), parent);
    QObject::connect(toggleHistogramView, &QAction::triggered, [getConfigPtr] () {
        auto config = getConfigPtr();
        if (config) {
            config->setDrawHistogramIsActive(!config->drawHistogramIsActive());
        }
    });

    QAction *toggleHistogramEq = new QAction(QIcon("://ressources/icons/contrast.png"), QObject::trUtf8("Activer l'équalisation par histogramme"), parent);
    QObject::connect(toggleHistogramEq, &QAction::triggered, [getConfigPtr] () {
        auto config = getConfigPtr();
        if (config) {
            config->setHistogramEqIsActive(!config->histogramEqIsActive());
        }
    });

    //toolbarActions.append(toggleView);
    toolbarActions.append(toggleTimecode);
    toolbarActions.append(toggleReticle);
    toolbarActions.append(toggleRecord);
    toolbarActions.append(toggleHistogramView);
    toolbarActions.append(toggleHistogramEq);

    menuActions.append(toggleGvsp);
    //menuActions.append(toggleView);
    menuActions.append(toggleRecord);
    menuActions.append(toggleTimecodeSettings);
    menuActions.append(toggleJpegRecorderSettings);
#ifdef XMP
    menuActions.append(toggleMetadatasSettings);
#endif
    menuActions.append(toggleContrastSettings);


}

QtReceiverSession::QtReceiverSession(QObject *parent)
    : CameraSession(*new QtReceiverSessionPrivate, parent)
{}

