find_package(GvspDevices REQUIRED)

if (GVSPDEVICES_FOUND)

    find_path(GVSPQTRECEIVER_INCLUDE_DIR NAMES gvspqtreceiver.h)
    mark_as_advanced(GVSPQTRECEIVER_INCLUDE_DIR)

    find_library(GVSPQTRECEIVER_LIBRARY NAMES gvspqtreceiver)
    mark_as_advanced(GVSPQTRECEIVER_LIBRARY)

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(GvspQtReceiver DEFAULT_MSG GVSPQTRECEIVER_INCLUDE_DIR GVSPQTRECEIVER_LIBRARY)

endif ()

