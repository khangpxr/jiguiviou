
find_path(GEVIPORT_INCLUDE_DIR NAMES geviport.h)
mark_as_advanced(GEVIPORT_INCLUDE_DIR)

find_library(GEVIPORT_LIBRARY NAMES geviport)
mark_as_advanced(GEVIPORT_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GevIPort DEFAULT_MSG GEVIPORT_INCLUDE_DIR GEVIPORT_LIBRARY)

#set(GEVIPORT_LINK_FLAG)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lgvcpdevices")

