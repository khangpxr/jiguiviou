
find_path(EXTERNSYNC_INCLUDE_DIR NAMES externsync.h)
mark_as_advanced(EXTERNSYNC_INCLUDE_DIR)

find_library(EXTERNSYNC_LIBRARY NAMES externsync)
mark_as_advanced(EXTERNSYN_LIBRARYC)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(externsync DEFAULT_MSG EXTERNSYNC_INCLUDE_DIR EXTERNSYNC_LIBRARY)

#set(GEVIPORT_LINK_FLAG)
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lgvcpdevices")

