
find_path(GVSPDEVICES_INCLUDE_DIR NAMES gvspdevices.h)
mark_as_advanced(GVSPDEVICES_INCLUDE_DIR)

find_library(GVSPDEVICES_LIBRARY NAMES
    gvspdevices
)
mark_as_advanced(GVSPDEVICES_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GvspDevices DEFAULT_MSG GVSPDEVICES_INCLUDE_DIR GVSPDEVICES_LIBRARY)



