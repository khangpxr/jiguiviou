
find_path(GLVIDEOWINDOW_INCLUDE_DIR NAMES glvideowindow.h)
mark_as_advanced(GLVIDEOWINDOW_INCLUDE_DIR)

find_library(GLVIDEOWINDOW_LIBRARY NAMES glvideowindow)
mark_as_advanced(GLVIDEOWINDOW_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GLVideoWindow DEFAULT_MSG GLVIDEOWINDOW_INCLUDE_DIR GLVIDEOWINDOW_LIBRARY)

#set(GEVIPORT_LINK_FLAG)
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lgvcpdevices")

