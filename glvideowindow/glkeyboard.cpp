/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glkeyboard.h"
#include "glkeyboard_p.h"

#include <QEvent>
#include <QMouseEvent>
#include <QPainter>

using namespace Jgv::GLWidget;


static const QVector<QPointF> SHIFT =
{ {0.,-2.},{2.,0.},{1.,0.},{1.,2.},{-1.,2.},{-1.,0.},{-2.,0.}};
static const QVector<QPointF> BACK =
{ {-4.,0.},{-2.,-2.},{3.,-2.},{3.,2.},{-2.,2.}};

void KeyboardPrivate::updateImage()
{
    WidgetPrivate::updateImage();

    QRectF rectF {rect};
    // détermine la taille d'une touche
    const QSizeF keySize { rectF.width() / 10., rectF.height() / 5. };
    QPointF topLeft {0., 0.};
    for (auto &key: line0.keys) {
        key.setSize(keySize);
        key.moveTopLeft(topLeft);
        topLeft.rx() += keySize.width();
    }
    line0.rectF = { line0.keys.front().topLeft(), line0.keys.back().bottomRight() };

    topLeft = { line0.keys.front().bottomLeft() };
    for (auto &key: line1.keys) {
        key.setSize(keySize);
        key.setSize(keySize);
        key.moveTopLeft(topLeft);
        topLeft.rx() += keySize.width();
    }
    line1.rectF = { line1.keys.front().topLeft(), line1.keys.back().bottomRight() };

    topLeft = { line1.keys.front().bottomLeft() };
    for (auto &key: line2.keys) {
        key.setSize(keySize);
        key.setSize(keySize);
        key.moveTopLeft(topLeft);
        topLeft.rx() += keySize.width();
    }
    line2.rectF = { line2.keys.front().topLeft(), line2.keys.back().bottomRight() };

    // la 4ème ligne est plus courte
    topLeft = { line2.keys.front().bottomLeft() };
    topLeft.rx() += keySize.width() * 1.5;
    for (auto &key: line3.keys) {
        key.setSize(keySize);
        key.setSize(keySize);
        key.moveTopLeft(topLeft);
        topLeft.rx() += keySize.width();
    }
    line3.rectF = { line3.keys.front().topLeft(), line3.keys.back().bottomRight() };

    topLeft = { line2.keys.front().bottomLeft() };
    topLeft.ry() += keySize.height();
    for (auto &key: line4.keys) {
        key.setSize(keySize);
        key.setSize(keySize);
        key.moveTopLeft(topLeft);
        topLeft.rx() += keySize.width();
    }
    line4.rectF = { line4.keys.front().topLeft(), line4.keys.back().bottomRight() };


    // shift remplit l'espace à gauche de la 3ème ligne
    shiftRect = { line2.keys.front().bottomLeft(), line3.keys.front().bottomLeft() };
    // backspace remplit l'espace à droite de la 3ème ligne
    backspaceRect = { shiftRect };
    backspaceRect.moveTopLeft(line3.keys.back().topRight() );



    // l'espace est à droite de dernière ligne
    spaceRect = {line4.rectF};
    spaceRect.moveTopLeft(line4.keys.back().topRight());

    // contruit l'image en monochrome
    image.fill(0);
    QPainter painter(&image);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(Qt::gray);
    painter.setFont(QFont("monospace", qRound(keySize.height() * 1. / 3.)));
    for (const auto &key: line0.keys) {
        painter.setPen(Qt::darkGray);
        painter.drawRoundedRect(key.adjusted( keySize.width()/20., keySize.height()/20.,
                                             -keySize.width()/20.,-keySize.height()/20.), 10., 10.);
        painter.setPen(Qt::white);
        painter.drawText(key, Qt::AlignCenter, QString(key.character[index]));
    }
    for (const auto &key: line1.keys) {
        painter.setPen(Qt::darkGray);
        painter.drawRoundedRect(key.adjusted( keySize.width()/20., keySize.height()/20.,
                                             -keySize.width()/20.,-keySize.height()/20.), 10., 10.);
        painter.setPen(Qt::white);
        painter.drawText(key, Qt::AlignCenter, QString(key.character[index]));
    }
    for (const auto &key: line2.keys) {
        painter.setPen(Qt::darkGray);
        painter.drawRoundedRect(key.adjusted( keySize.width()/20., keySize.height()/20.,
                                             -keySize.width()/20.,-keySize.height()/20.), 10., 10.);
        painter.setPen(Qt::white);
        painter.drawText(key, Qt::AlignCenter, QString(key.character[index]));
    }
    for (const auto &key: line3.keys) {
        painter.setPen(Qt::darkGray);
        painter.drawRoundedRect(key.adjusted( keySize.width()/20., keySize.height()/20.,
                                             -keySize.width()/20.,-keySize.height()/20.), 10., 10.);
        painter.setPen(Qt::white);
        painter.drawText(key, Qt::AlignCenter, QString(key.character[index]));
    }
    for (const auto &key: line4.keys) {
        painter.setPen(Qt::darkGray);
        painter.drawRoundedRect(key.adjusted( keySize.width()/20., keySize.height()/20.,
                                             -keySize.width()/20.,-keySize.height()/20.), 10., 10.);
        painter.setPen(Qt::white);
        painter.drawText(key, Qt::AlignCenter, QString(key.character[index]));
    }

    // dessin du shift
    QVector<QPointF> shift {SHIFT};
    for (auto &point: shift) {
        point = point * (shiftRect.height() / 10.0);
    }
    painter.setPen(Qt::white);
    painter.save();
    if (index > 0) {
        painter.setBrush(Qt::white);
    }
    painter.translate(shiftRect.center());
    painter.drawPolygon(shift);
    painter.restore();

    // dessin du backspace
    QVector<QPointF> back {BACK};
    for (auto &point: back) {
        point = point * (backspaceRect.height() / 10.0);
    }
    painter.save();
    painter.translate(backspaceRect.center());
    painter.setPen(Qt::darkGray);
    painter.drawPolygon(back);
    painter.restore();
    painter.setFont(QFont("monospace", qRound(backspaceRect.height() / 4.)));
    painter.drawText(backspaceRect, Qt::AlignCenter, "X");


    // l'espace
    painter.setPen(Qt::darkGray);
    painter.setBrush(Qt::lightGray);
    painter.drawRoundedRect(spaceRect.adjusted( keySize.width()/10., keySize.height()/10.,
                                                -keySize.width()/10.,-keySize.height()/10.), 15., 15.);

}


Keyboard::Keyboard()
    : Widget(*new KeyboardPrivate)
{
    IMPL(Keyboard);

    d->string.reserve(20);

    d->line0.keys[0].character = { {'1', '!'} };
    d->line0.keys[1].character = { {'2', '@'} };
    d->line0.keys[2].character = { {'3', '#'} };
    d->line0.keys[3].character = { {'4', '$'} };
    d->line0.keys[4].character = { {'5', '%'} };
    d->line0.keys[5].character = { {'6', '^'} };
    d->line0.keys[6].character = { {'7', '&'} };
    d->line0.keys[7].character = { {'8', '*'} };
    d->line0.keys[8].character = { {'9', '('} };
    d->line0.keys[9].character = { {'0', ')'} };
    d->line1.keys[0].character = { {'a', 'A'} };
    d->line1.keys[1].character = { {'z', 'Z'} };
    d->line1.keys[2].character = { {'e', 'E'} };
    d->line1.keys[3].character = { {'r', 'R'} };
    d->line1.keys[4].character = { {'t', 'T'} };
    d->line1.keys[5].character = { {'y', 'Y'} };
    d->line1.keys[6].character = { {'u', 'U'} };
    d->line1.keys[7].character = { {'i', 'I'} };
    d->line1.keys[8].character = { {'o', 'O'} };
    d->line1.keys[9].character = { {'p', 'P'} };
    d->line2.keys[0].character = { {'q', 'Q'} };
    d->line2.keys[1].character = { {'s', 'S'} };
    d->line2.keys[2].character = { {'d', 'D'} };
    d->line2.keys[3].character = { {'f', 'F'} };
    d->line2.keys[4].character = { {'g', 'G'} };
    d->line2.keys[5].character = { {'h', 'H'} };
    d->line2.keys[6].character = { {'j', 'J'} };
    d->line2.keys[7].character = { {'k', 'K'} };
    d->line2.keys[8].character = { {'l', 'L'} };
    d->line2.keys[9].character = { {'m', 'M'} };
    d->line3.keys[0].character = { {'w', 'W'} };
    d->line3.keys[1].character = { {'x', 'X'} };
    d->line3.keys[2].character = { {'c', 'C'} };
    d->line3.keys[3].character = { {'v', 'V'} };
    d->line3.keys[4].character = { {'b', 'B'} };
    d->line3.keys[5].character = { {'n', 'N'} };
    d->line3.keys[6].character = { {'\'', '?'} };
    d->line4.keys[0].character = { {'+', '+'} };
    d->line4.keys[1].character = { {'-', '-'} };
    d->line4.keys[2].character = { {'*', '*'} };
    d->line4.keys[3].character = { {'/', '/'} };


}
Keyboard::~Keyboard() = default;

const QString &Keyboard::string() const
{
    IMPL(const Keyboard);
    return d->string;
}

void Keyboard::clearString()
{
    IMPL(Keyboard);
    d->string.clear();
}

int Keyboard::columns() const
{
    return 10;
}

int Keyboard::lines() const
{
    return 5;
}

bool Keyboard::event(QEvent *event)
{
    IMPL(Keyboard);

    if (event->type() == QEvent::MouseButtonRelease) {
        auto mouseEvent = static_cast<QMouseEvent *>(event);
        if (mouseEvent->button() == Qt::LeftButton) {
            auto pos = mouseEvent->pos();
            if (d->rect.contains(pos)) {
                // transpose la position dans le système de l'image
                pos.rx() -= d->rect.x();
                pos.ry() -= d->rect.y();

                auto posToString = [d](const QPoint &pos, const std::vector<Key> &keys) {
                    for (const auto &key: keys) {
                        if (key.contains(pos)) {
                            d->string.append(key.character[d->index]);
                        }
                    }
                };

                if (d->line0.rectF.contains(pos)) posToString(pos, d->line0.keys);
                else if (d->line1.rectF.contains(pos)) posToString(pos, d->line1.keys);
                else if (d->line2.rectF.contains(pos)) posToString(pos, d->line2.keys);
                else if (d->line3.rectF.contains(pos)) posToString(pos, d->line3.keys);
                else if (d->line4.rectF.contains(pos)) posToString(pos, d->line4.keys);
                else if (d->shiftRect.contains(pos)) {
                    d->index = (1+d->index) % 2;
                    d->imageNeedUpdate = true;
                }
                else if (d->spaceRect.contains(pos)) d->string.append(' ');
                else if (d->backspaceRect.contains(pos)) d->string.remove(d->string.size() - 1, 1);
                return true;
            }
        }
    }
    return false;
}








