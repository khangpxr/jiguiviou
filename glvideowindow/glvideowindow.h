/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLVIDEOWINDOW_H
#define GLVIDEOWINDOW_H

#include "pimpl.hpp"
#include "gvspqtreceiver.h"

#include <QObject>
#include <QOpenGLWindow>


namespace Jgv {

class GLWindowWrapper : public QObject, public Jgv::Gvsp::QtBackend::GLVideoRenderer
{
    Q_OBJECT
public:
    void setTextureToRender(GLuint texture) override;
signals:
    void textureChanged(GLuint texture);
};

class GLVideoWindowPrivate;
class GLVideoWindow : public QOpenGLWindow
{
    Q_OBJECT
public:
    explicit GLVideoWindow(QOpenGLContext *shareContext);
    virtual ~GLVideoWindow() override;

    void setReceiverConfig(QWeakPointer<Jgv::Gvsp::QtBackend::ReceiverConfig> config);

    QWeakPointer<Jgv::Gvsp::QtBackend::GLVideoRenderer> wrapper();

signals:
    void changeWindowState();

private:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    bool event(QEvent *event) override;
private:
    BASE_PIMPL(GLVideoWindow)


}; // class VideoWindow

} // namespace VTO_2

#endif // GLVIDEOWINDOW_H
