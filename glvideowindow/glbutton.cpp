/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glbutton.h"
#include "glbutton_p.h"

#include <QPainter>
#include <QEvent>
#include <QMouseEvent>

using namespace Jgv::GLWidget;

void ButtonPrivate::updateImage()
{
    WidgetPrivate::updateImage();

    QRectF rectF { rect };
    rectF.moveTopLeft( {0., 0.} );
    const qreal decX = rectF.width() * 0.1;
    const qreal decY = rectF.height() * 0.1;

    image.fill(Qt::transparent);
    QPainter painter(&image);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::darkGray);
    painter.setBrush(Qt::gray);
    painter.drawRoundedRect(rectF, 10., 10.);
    painter.setPen(Qt::gray);
    painter.setBrush(Qt::darkGray);
    painter.drawEllipse(rectF.adjusted(decX, decY, -decX, -decY));

    switch (type) {
    case ButtonType::Record:
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 4.)));
        painter.drawText(rectF, Qt::AlignCenter, "REC");
        break;
    case ButtonType::Reticule:
        painter.setBrush(Qt::NoBrush);
        painter.setPen(Qt::lightGray);
        painter.drawRect( {QPointF(1.*decX, 1.*decY), QPointF(9.*decX, 9.*decY)});
        painter.drawLine(QPointF(1.*decX, 5.*decY), QPointF(9.*decX, 5.*decY));
        painter.drawLine(QPointF(5.*decX, 1.*decY), QPointF(5.*decX, 9.*decY));
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 4.)));
        painter.drawText(rectF, Qt::AlignCenter, "R");
        break;
    case ButtonType::TimeCode:
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 8.)));
        painter.drawText(rectF, Qt::AlignBottom | Qt::AlignHCenter, "12:20:45");
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 4.)));
        painter.drawText(rectF, Qt::AlignCenter, "TC");
        break;
    case ButtonType::Histogram:
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 4.)));
        painter.drawText(rectF, Qt::AlignCenter, "H");
        break;
    case ButtonType::Equalization:
        painter.setBrush(Qt::lightGray);
        painter.drawPie(rectF.adjusted(decX, decY, -decX, -decY), 90*16, 180 *16);
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 4.)));
        painter.drawText(rectF, Qt::AlignCenter, "Eq");
        break;
    case ButtonType::More:
    {
        QRectF rect { 2.*decX, 2.5*decY, 6.*decX, 1.*decY };
        painter.setBrush(Qt::white);
        painter.drawRect( rect );
        rect.translate(0., 2.*decY);
        painter.drawRect( rect );
        rect.translate(0., 2.*decY);
        painter.drawRect( rect );
        break;
    }
    case ButtonType::BlackLevel:
    {
        QRectF rect { 1.*decX, 5.*decY, 2.*decX, 4.*decY };
        painter.setBrush(Qt::black);
        painter.drawRect( rect );
        rect.translate(3.*decX, 0.);
        rect.setTop(rect.top() - 1.5*decY);
        painter.drawRect( rect );
        rect.translate(3.*decX, 0.);
        rect.setTop(rect.top() - 1.5*decY);
        painter.drawRect( rect );
        break;
    }
    case ButtonType::BlackLevelIn:
    {
        QRectF rect { 1.*decX, 5.*decY, 2.*decX, 4.*decY };
        painter.setBrush(Qt::black);
        painter.drawRect( rect );
        rect.translate(3.*decX, 0.);
        rect.setTop(rect.top() - 1.5*decY);
        painter.drawRect( rect );
        rect.translate(3.*decX, 0.);
        rect.setTop(rect.top() - 1.5*decY);
        painter.drawRect( rect );
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 2.)));
        painter.drawText(rectF, Qt::AlignCenter, "E");
        break;
    }
    case ButtonType::BlackLevelOut:
    {
        QRectF rect { 1.*decX, 5.*decY, 2.*decX, 4.*decY };
        painter.setBrush(Qt::black);
        painter.drawRect( rect );
        rect.translate(3.*decX, 0.);
        rect.setTop(rect.top() - 1.5*decY);
        painter.drawRect( rect );
        rect.translate(3.*decX, 0.);
        rect.setTop(rect.top() - 1.5*decY);
        painter.drawRect( rect );
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", qRound(rectF.height() * 1. / 2.)));
        painter.drawText(rectF, Qt::AlignCenter, "S");
        break;
    }
    case ButtonType::Return:
    {
        std::array<QPointF, 9> points {
            { {1.*decX, 5.5*decY}, {3.5*decX, 3.*decY}, {3.5*decX, 4.*decY},
                {6.*decX, 4.*decY}, {6.*decX, 2.*decY}, {8.*decX, 2.*decY},
                {8.*decX, 7.*decY}, {3.5*decX, 7.*decY}, {3.5*decX, 8.*decY} }
        };
        painter.setBrush(Qt::white);
        painter.drawPolygon(points.data(), points.size());
        break;
    }

    case ButtonType::None:
        break;
    }


}

void ButtonPrivate::updateMatrix()
{
    WidgetPrivate::updateMatrix();
    matrixOn = matrix;
    matrixOn.scale(0.9f);
}

Button::Button()
    : Widget(*new ButtonPrivate)
{
    IMPL(Button);
    d->type = ButtonType::None;
}

void Button::setButtonType(ButtonType type)
{
    IMPL(Button);
    d->type = type;
}

void Button::setNotify(std::shared_ptr<Notify> notify)
{
    IMPL(Button);
    d->notify = notify;
}

const QMatrix4x4 &Button::transform() const
{
    IMPL(const Button);
    return d->isOn?d->matrixOn:d->matrix;
}

Button::~Button() = default;


bool Button::event(QEvent *event)
{
    IMPL(Button);



    if (event->type() == QEvent::MouseButtonRelease) {
        auto mouseEvent = static_cast<QMouseEvent *>(event);
        if (mouseEvent->button() == Qt::LeftButton) {

            if (d->rect.contains(mouseEvent->pos())) {
                d->isOn = false;
                if (d->notify) {
                    d->notify->clicked();
                }

                return true;
            }
        }

    }
    if (event->type() == QEvent::MouseButtonPress) {
        auto mouseEvent = static_cast<QMouseEvent *>(event);
        if (mouseEvent->button() == Qt::LeftButton) {
            if (d->rect.contains(mouseEvent->pos())) {
                d->isOn = true;
                return true;
            }
        }
    }
    if (event->type() == QEvent::MouseMove) {
        auto mouseEvent = static_cast<QMouseEvent *>(event);
        if (!d->rect.contains(mouseEvent->pos())) {
            d->isOn = false;
        }
    }
    return false;
}









