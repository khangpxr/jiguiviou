/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glwidget.h"
#include "glwidget_p.h"

using namespace Jgv::GLWidget;

void WidgetPrivate::updateMatrix()
{
    const QSizeF viewportSizeF { viewportSize };

    // on retourne les coordonnées car en GL, c'est à l'envers
    QRectF rectF {rect};
    rectF.moveBottom(rectF.height() - rectF.top());

    matrix.setToIdentity();
    // applique la position relative
    matrix.translate( static_cast<float>(2.0 * rectF.topLeft().x()/viewportSizeF.width()), static_cast<float>(2.0 * rectF.topLeft().y()/viewportSizeF.height()) );
    // déplace en (0,0) écran le TopLeft du label
    matrix.translate(static_cast<float>((rectF.width()/viewportSizeF.width())-1.), static_cast<float>(1.-(rectF.height()/viewportSizeF.height())));
    // mise à l'échelle
    matrix.scale(static_cast<float>(rectF.width() / viewportSizeF.width()),
                 static_cast<float>(-rectF.height() / viewportSizeF.height()));

}

void WidgetPrivate::updateImage()
{
    if (image.size() != rect.size()) {
        image = QImage( rect.size(), QImage::Format_ARGB32 );
    }
}


Widget::Widget(WidgetPrivate &dd)
    : p_impl(&dd)
{}

void Widget::setSizes(const QSize &widgetSize, const QSize &viewportSize)
{
    IMPL(Widget);
    d->rect.setSize(widgetSize);
    d->viewportSize = viewportSize;
    d->imageNeedUpdate = true;
    d->updateMatrix();
}
Widget::~Widget() = default;

QSize Widget::size() const
{
    IMPL(const Widget);
    return d->rect.size();
}

void Widget::setPosition(const QPoint &position)
{
    IMPL(Widget);
    d->rect.moveTopLeft(position);
    d->updateMatrix();
}

QPoint Widget::position() const
{
    IMPL(const Widget);
    return d->rect.topLeft();
}

QPoint Widget::bottomRight() const
{
    IMPL(const Widget);
    return d->rect.bottomRight();
}

const QMatrix4x4 &Widget::transform() const
{
    IMPL(const Widget);
    return d->matrix;
}

bool Widget::imageChanged() const
{
    IMPL(const Widget);
    return d->imageNeedUpdate;
}

QImage Widget::image()
{
    IMPL(Widget);
    if (d->imageNeedUpdate) {
        d->updateImage();
        d->imageNeedUpdate = false;
    }

    return d->image;
}

bool Widget::event(QEvent *event)
{
    Q_UNUSED(event)
    return false;
}







