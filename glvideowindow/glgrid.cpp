/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glgrid.h"
#include "glgrid_p.h"

#include "glbutton.h"

#include <QSize>
#include <QPoint>

using namespace Jgv::GLContainer;

Grid::Grid(unsigned lines, unsigned columns)
    : p_impl(new GridPrivate)
{
    IMPL(Grid);
    d->lines = lines;
    d->columns = columns;
}

Grid::~Grid() = default;

void Grid::alignWidget(Jgv::GLWidget::Widget &widget) const
{
    IMPL(const Grid);

    double width, height;
    switch (widget.gridRect.width()) {
    case 0:
        width = 1;
        break;
    case 1:
        width = qMin(d->elementSize.width(), d->elementSize.height());
        break;
    default:
        width = 2 * (d->elementSize.width() + d->marginX) + (widget.gridRect.width() - 2) * (d->celluleSize.width());
    }
    switch (widget.gridRect.height()) {
    case 0:
        height = 1;
        break;
    case 1:
        height = qMin(d->elementSize.width(), d->elementSize.height());
        break;
    default:
        height = 2 * (d->elementSize.height() + d->marginY) + (widget.gridRect.height() - 2) * (d->celluleSize.height());
    }

    widget.setSizes( {qRound(width), qRound(height)}, d->viewSize);
    widget.setPosition( {qRound(d->marginX + widget.gridRect.x() * d->celluleSize.width()),
                         qRound(d->marginY + widget.gridRect.y() * d->celluleSize.height())} ) ;
}



void Grid::setViewSize(int w, int h)
{
    IMPL(Grid);

    d->viewSize.setWidth(w);
    d->viewSize.setHeight(h);
    d->celluleSize.setWidth(static_cast<double>(w) / static_cast<double>(d->columns));
    d->celluleSize.setHeight(static_cast<double>(h) / static_cast<double>(d->lines));
    d->marginX = d->celluleSize.width() / 7.;
    d->marginY = d->celluleSize.height() / 7.;
    d->elementSize.setWidth(d->marginX * 5.);
    d->elementSize.setHeight(d->marginY * 5.);

}





