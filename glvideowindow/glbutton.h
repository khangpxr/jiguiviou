/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLBUTTON_H
#define GLBUTTON_H

#include "glwidget.h"



class QPointF;
class QSizeF;
class QMatrix4x4;
class QString;
class QEvent;

namespace Jgv {

namespace GLWidget {

struct Notify {
    virtual ~Notify() = default;
    virtual void clicked() = 0;
};

enum class ButtonType {
    None, Record, Reticule, TimeCode, Histogram, Equalization, More,
    BlackLevel, BlackLevelIn, BlackLevelOut, Return
};

class ButtonPrivate;
class Button : public Widget
{

public:
    Button();
    virtual ~Button() override;
    void setButtonType(ButtonType type);
    void setNotify(std::shared_ptr<Notify> notify);
    const QMatrix4x4 &transform() const override;

    bool event(QEvent *event) override;


private:
    PIMPL(Button)
}; // class Button

} // namespace GLWidget;

} // namespace Jgv

#endif // GLBUTTON_H
