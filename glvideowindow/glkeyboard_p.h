/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLKEYBOARD_P_H
#define GLKEYBOARD_P_H

#include "glwidget_p.h"

#include <QImage>

#include <QSizeF>
#include <QRectF>
#include <QMatrix4x4>
#include <memory>

namespace Jgv {

namespace GLWidget {


class Key : public QRectF
{
public:
    std::array<char, 8> character{ {0,0,0,0,0,0,0,0} };
};

struct KeysLine
{
    std::vector<Key> keys;
    QRectF rectF;
};

class KeyboardPrivate final : public WidgetPrivate
{
public:
    virtual ~KeyboardPrivate() override = default;

    QString string;
    std::size_t index {0};
    KeysLine line0 {std::vector<Key> {10}, QRect()};
    KeysLine line1 {std::vector<Key> {10}, QRect()};
    KeysLine line2 {std::vector<Key> {10}, QRect()};
    KeysLine line3 {std::vector<Key> {7}, QRect()};
    KeysLine line4 {std::vector<Key> {4}, QRect()};
    QRectF shiftRect;
    QRectF backspaceRect;
    QRectF spaceRect;


    virtual void updateImage() override final;
}; // class KeyboardPrivate

} // namespace GLWidget

} // namespace Jgv



#endif // GLKEYBOARD_P_H
