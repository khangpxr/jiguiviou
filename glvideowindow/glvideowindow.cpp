/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glvideowindow.h"
#include "glvideowindow_p.h"

#include "shaders.h"
#include "gvspqtreceiverconfig.h"


using namespace Jgv;

#include <QOpenGLShader>
#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions>
#include <QMutexLocker>
#include <QMouseEvent>


void GLVideoWindowPrivate::updateTextureSize(const QSizeF &viewSize)
{
    // on a besoin de la taille de la texture

    GLint width = 0;
    GLint height = 0;
    functions->glBindTexture(GL_TEXTURE_RECTANGLE, texture);
    functions->glGetTexLevelParameteriv(GL_TEXTURE_RECTANGLE, 0, GL_TEXTURE_WIDTH, &width);
    functions->glGetTexLevelParameteriv(GL_TEXTURE_RECTANGLE, 0, GL_TEXTURE_HEIGHT, &height);
    functions->glBindTexture(GL_TEXTURE, 0);
    if (width>0 && height>0) {

        // la taille de la texture agrandie aux limites de la vue
        QSizeF textureToView { QSizeF(width, height).scaled(viewSize, Qt::KeepAspectRatio) };
        // on normalise
        textureToView.rwidth() /= viewSize.width();
        textureToView.rheight() /= viewSize.height();

        // met à jour la matrice de transformation
        transformTexture.setToIdentity();
        transformTexture.scale(static_cast<float>(textureToView.width()), static_cast<float>(-1.0 * textureToView.height()));
        textureSize.setX(width);
        textureSize.setY(height);
        programPtr->bind();
        programPtr->setUniformValue("transform", transformTexture);
        programPtr->setUniformValue("texSize", textureSize);
        programPtr->release();

    }


}

void GLWindowWrapper::setTextureToRender(GLuint texture)
{
    emit textureChanged(texture);
}

GLVideoWindow::GLVideoWindow(QOpenGLContext *shareContext)
    : QOpenGLWindow(shareContext)
    , p_impl(new GLVideoWindowPrivate)
{
    IMPL(GLVideoWindow);

    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    setFormat(fmt);

    d->videoWrapper.reset(new GLWindowWrapper);
    connect(d->videoWrapper.data(),&GLWindowWrapper::textureChanged, [d](GLuint texture) {
        d->texture = texture;
        d->textureSizeNeedUpdate = true;
    });

}

GLVideoWindow::~GLVideoWindow()
{
    IMPL(GLVideoWindow);
    makeCurrent();
    d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, 0);
    if (d->programPtr) {
        d->programPtr->removeAllShaders();
    }

    if (d->overlayProgramPtr) {
        d->overlayProgramPtr->removeAllShaders();
    }

    std::function<void (std::shared_ptr<Jgv::GLContainer::Page> )> cleanPage;
    cleanPage = [&cleanPage] (std::shared_ptr<Jgv::GLContainer::Page> page) {
        for (auto &widget: page->widgets()) {
            if (widget->texture) {
                widget->texture->release();
                widget->texture->destroy();
                widget->texture.reset();
            }
        }
        if (page->haveChilds()) {
            for (auto &child: page->childs()) {
                cleanPage(child);
            }
        }
    };
    cleanPage(d->page);


    doneCurrent();
}

void GLVideoWindow::setReceiverConfig(QWeakPointer<Jgv::Gvsp::QtBackend::ReceiverConfig> config)
{
    IMPL(GLVideoWindow);
    d->page->setReceiverConfig(config);
}

QWeakPointer<Jgv::Gvsp::QtBackend::GLVideoRenderer> GLVideoWindow::wrapper()
{
    IMPL(GLVideoWindow);
    return d->videoWrapper.toWeakRef();
}

void GLVideoWindow::initializeGL()
{
    IMPL(GLVideoWindow);
    d->functions = context()->versionFunctions<QOpenGLFunctions_4_2_Core>();
    if (d->functions == nullptr) {
        qWarning("VideoWidget::initialize : Could not obtain required OpenGL context version");
        return;
    }

    d->programPtr.reset(new QOpenGLShaderProgram);
    d->programPtr->addShaderFromSourceCode(QOpenGLShader::Vertex, Shaders::mainTextureVertex());
    d->programPtr->addShaderFromSourceCode(QOpenGLShader::Fragment, Shaders::mainTextureFragment());
    d->programPtr->link();

    d->overlayProgramPtr.reset(new QOpenGLShaderProgram);
    d->overlayProgramPtr->addShaderFromSourceCode(QOpenGLShader::Vertex, Shaders::glWidgetVertex());
    d->overlayProgramPtr->addShaderFromSourceCode(QOpenGLShader::Fragment, Shaders::glWidgetFragment());
    d->overlayProgramPtr->link();


    QMatrix4x4 transform;
    transform.setToIdentity();
    transform.scale(1.0f, -1.0f);
    d->programPtr->bind();
    d->programPtr->setUniformValue("transform", transform);
    d->programPtr->setUniformValue("texSize", QVector2D(10, 10));
    d->programPtr->release();

    d->overlayProgramPtr->bind();
    d->overlayProgramPtr->setUniformValue("transform", transform);
    d->overlayProgramPtr->setUniformValue("texSize", QVector2D(10.f, 10.f));
    d->overlayProgramPtr->setUniformValue("color", QVector3D(1.f, 1.f, 1.f));
    d->overlayProgramPtr->release();


    std::array<GLfloat, 16> coords {
        {  -1.0f, -1.0f, 0.0f, 0.0f,
            1.0f, -1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 0.0f, 1.0f}
    };
    d->vbo.create();
    d->vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    d->vbo.bind();
    d->vbo.allocate(coords.data(), coords.size() * sizeof(GLfloat));
    d->vbo.release();

    std::array<GLushort, 6> indices { {0, 1, 2, 0, 2, 3} };
    d->indicesVbo.create();
    d->indicesVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    d->indicesVbo.bind();
    d->indicesVbo.allocate(indices.data(), indices.size() * sizeof(GLushort));
    d->indicesVbo.release();

    d->vao.create();
    d->vao.bind();
    d->vbo.bind();
    d->indicesVbo.bind();
    d->functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0));
    d->functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(2 * sizeof (GLfloat)));
    d->functions->glEnableVertexAttribArray(0);
    d->functions->glEnableVertexAttribArray(1);

    d->vao.release();

}

void GLVideoWindow::resizeGL(int w, int h)
{
    IMPL(GLVideoWindow);

    d->updateTextureSize( {static_cast<qreal>(w), static_cast<qreal>(h)} );

    d->page->setViewSize(w, h);

    // le clavier est en bas
    const int kbWidth = w * 6 / 10;
    d->keyboard.keyboard.setSizes({kbWidth, kbWidth * d->keyboard.keyboard.lines() / d->keyboard.keyboard.columns() }, {w,h});
    d->keyboard.keyboard.setPosition( {(w - d->keyboard.keyboard.size().width() ) / 2, h - d->keyboard.keyboard.size().height() } );
    d->keyboard.texture.reset();

    // on positionne le label au dessus du clavier,
    // au milieu de la surface restante
    d->label.label.setSizes({w/2, h/10}, {w,h});
    const auto keyboardSize = d->keyboard.keyboard.size();
    const auto labelSize = d->label.label.size();
    d->label.label.setPosition( {(w-labelSize.width())/2, (h-keyboardSize.height()-labelSize.height())/2} );
    d->label.texture.reset();

}

void GLVideoWindow::paintGL()
{
    IMPL(GLVideoWindow);

    if (d->functions == nullptr) {
        qWarning("GLVideoWindow: trying to paint with GL not initialized");
        return;
    }

    if (d->textureSizeNeedUpdate) {
        d->textureSizeNeedUpdate = false;
        d->updateTextureSize(size());
    }

    d->programPtr->bind();
    d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, d->texture);
    d->functions->glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    d->functions->glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    d->functions->glClearColor(0.192f, 0.212f, 0.231f, 1.0f);
    d->functions->glClear(GL_COLOR_BUFFER_BIT);
    d->vao.bind();
    d->functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    d->vao.release();
    d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, 0);
    d->programPtr->release();


    auto updateWidget = [](Jgv::GLWidget::Widget &widget) {
        const QImage image = widget.image();
        if (QSize { widget.texture->width(), widget.texture->height() } == image.size() ) {
            widget.texture->bind();
            widget.texture->setData(QOpenGLTexture::BGRA, QOpenGLTexture::UInt8, image.bits());
            widget.texture->release();
        }
        else {
            qWarning("GLVideoWindow: can't update glwidget, sizes difeers");
        }
    };

    auto drawWidget = [d](Jgv::GLWidget::Widget &widget) {
        d->overlayProgramPtr->bind();
        d->overlayProgramPtr->setUniformValue("transform", widget.transform());
        d->overlayProgramPtr->setUniformValue("texSize", QVector2D(widget.size().width(), widget.size().height()));
        d->overlayProgramPtr->setUniformValue("alpha", 0.9f);
        d->vao.bind();
        widget.texture->bind();
        d->functions->glEnable(GL_BLEND);
        d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        d->functions->glBlendEquation (GL_FUNC_ADD);
        d->functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
        d->functions->glDisable(GL_BLEND);
        widget.texture->release();
        d->vao.release();
        d->overlayProgramPtr->release();
    };

    auto createTexture = [](Jgv::GLWidget::Widget &widget) {
        widget.texture.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
        widget.texture->setAutoMipMapGenerationEnabled(false);
        widget.texture->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
        widget.texture->setSize(widget.size().width(), widget.size().height());
        widget.texture->setFormat(QOpenGLTexture::RGBA8_UNorm);
        widget.texture->allocateStorage();
    };

    auto doWidget = [&createTexture, &updateWidget, &drawWidget](Jgv::GLWidget::Widget &widget) {
        if (!widget.texture) {
            createTexture(widget);
            updateWidget(widget);
        }
        if (widget.imageChanged()) {
            updateWidget(widget);
        }
        drawWidget(widget);
    };


    std::function<void (std::shared_ptr<Jgv::GLContainer::Page> )> cleanPage;
    cleanPage = [&cleanPage] (std::shared_ptr<Jgv::GLContainer::Page> page) {
        for (auto &widget: page->widgets()) {
            if (widget->texture) {
                widget->texture->release();
                widget->texture->destroy();
                widget->texture.reset();
            }
        }
        if (page->haveChilds()) {
            for (auto &child: page->childs()) {
                cleanPage(child);
            }
        }
    };


    std::function<bool (std::shared_ptr<Jgv::GLContainer::Page> )> doPage;
    doPage = [&doWidget, &doPage] (std::shared_ptr<Jgv::GLContainer::Page> page) {
        if (page->isVisible()) {
            for (auto &widget: page->widgets()) {
                doWidget(*widget.get());
            }
            return true;
        }
        else if (page->haveChilds()) {
            for (auto &child: page->childs()) {
                if (doPage(child)) {
                    return true;
                }
            }
        }
        return false;
    };

    if (!doPage(d->page)) {
        cleanPage(d->page);
    }





    //        if (d->drawButtons) {

    //            auto updateKeyboard = [d]() {
    //                const QImage image = d->keyboard.keyboard.image();
    //                if (QSize { d->keyboard.texture->width(), d->keyboard.texture->height() } == image.size() ) {
    //                    d->keyboard.texture->bind();
    //                    d->keyboard.texture->setData(QOpenGLTexture::Red, QOpenGLTexture::UInt8, image.bits());
    //                    d->keyboard.texture->release();
    //                }
    //                else {
    //                    qWarning("GLVideoWindow: can't update keyboard texture, sizes difeers");
    //                }
    //            };

    //            auto updateLabel = [d]() {
    //                const QImage image = d->label.label.image();
    //                if (QSize { d->label.texture->width(), d->label.texture->height() } == image.size() ) {
    //                    d->label.texture->bind();
    //                    d->label.texture->setData(QOpenGLTexture::Red, QOpenGLTexture::UInt8, image.bits());
    //                    d->label.texture->release();
    //                }
    //                else {
    //                    qWarning("GLVideoWindow: can't update keyboard texture, sizes difeers");
    //                }
    //            };

    //            if (!d->keyboard.texture) {
    //                d->keyboard.texture.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
    //                d->keyboard.texture->setAutoMipMapGenerationEnabled(false);
    //                d->keyboard.texture->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
    //                d->keyboard.texture->setSize(d->keyboard.keyboard.size().width(), d->keyboard.keyboard.size().height());
    //                d->keyboard.texture->setFormat(QOpenGLTexture::R8_UNorm);
    //                d->keyboard.texture->allocateStorage();
    //                updateKeyboard();
    //            }
    //            if (!d->label.texture) {
    //                d->label.texture.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
    //                d->label.texture->setAutoMipMapGenerationEnabled(false);
    //                d->label.texture->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
    //                d->label.texture->setSize(d->label.label.size().width(), d->label.label.size().height());
    //                d->label.texture->setFormat(QOpenGLTexture::R8_UNorm);
    //                d->label.texture->allocateStorage();
    //                updateLabel();
    //            }

    //            if (d->keyboard.keyboard.imageChanged()) {
    //                updateKeyboard();
    //            }

    //            d->overlayProgramPtr->bind();
    //            d->vao.bind();
    //            d->overlayProgramPtr->setUniformValue("transform", d->keyboard.keyboard.transform());
    //            d->overlayProgramPtr->setUniformValue("texSize", QVector2D(d->keyboard.keyboard.size().width(), d->keyboard.keyboard.size().height()));
    //            d->overlayProgramPtr->setUniformValue("alpha", 1.0f);
    //            d->keyboard.texture->bind();
    //            d->functions->glEnable(GL_BLEND);
    //            d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //            d->functions->glBlendEquation (GL_FUNC_ADD);
    //            d->functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    //            d->functions->glDisable(GL_BLEND);
    //            d->keyboard.texture->release();

    //            d->label.label.setString(d->keyboard.keyboard.string());
    //            if (d->label.label.imageChanged()) {
    //                updateLabel();
    //            }

    //            d->overlayProgramPtr->setUniformValue("transform", d->label.label.transform());
    //            d->overlayProgramPtr->setUniformValue("texSize", QVector2D(d->label.texture->width(), d->label.texture->height()));
    //            d->label.texture->bind();
    //            d->functions->glEnable(GL_BLEND);
    //            d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //            d->functions->glBlendEquation (GL_FUNC_ADD);
    //            d->functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    //            d->functions->glDisable(GL_BLEND);
    //            d->label.texture->release();

    //            d->vao.release();
    //            d->overlayProgramPtr->release();


    //        }
    //        else {
    //            if (d->keyboard.texture) {
    //                d->keyboard.texture->destroy();
    //                d->keyboard.texture.reset();
    //            }
    //            if (d->label.texture) {
    //                d->label.texture->destroy();
    //                d->label.texture.reset();
    //            }
    //        }




    update();
}

bool GLVideoWindow::event(QEvent *event)
{
    IMPL(GLVideoWindow);

    auto isMouseEvent = [](QEvent *event) {
        return event->type() == QEvent::MouseButtonPress
                || event->type() == QEvent::MouseButtonRelease
                || event->type() == QEvent::MouseMove;
    };



    if (isMouseEvent(event)) {

        if (d->page->event(event)) {
            return true;
        }
        else if (d->swipe.direction(event) == SwipeDirection::right) {
            emit changeWindowState();
        }
        else if (event->type() == QEvent::MouseButtonRelease) {
            d->page->setVisible(true);
        }
        return true;
    }


    return QOpenGLWindow::event(event);
}





