/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpage.h"
#include "glpage_p.h"

using namespace Jgv::GLContainer;

Page::Page(PagePrivate &dd)
    : p_impl(&dd)
{}

Page::~Page() = default;

void Page::setParent(Page *parent)
{
    IMPL(Page);
    d->parent = parent;
}


void Page::setReceiverConfig(QWeakPointer<Jgv::Gvsp::QtBackend::ReceiverConfig> config)
{
    IMPL(Page);
    d->config = config;
    for (auto &child: d->childs) {
        child->setReceiverConfig(config);
    }
}

void Page::setViewSize(int w, int h)
{
    IMPL(Page);
    // placement
    d->grid.setViewSize(w, h);
    for (auto &widget: d->widgets) {
        auto &w = *widget.get();
        d->grid.alignWidget(w);
    }
    for (auto &page: d->childs) {
        page->setViewSize(w, h);
    }
}




bool Page::isVisible() const
{
    IMPL(const Page);
    return d->isVisible;
}

void Page::setVisible(bool visible)
{
    IMPL(Page);
    d->isVisible = visible;
}

bool Page::haveChilds() const
{
    IMPL(const Page);
    return !d->childs.empty();
}

const std::vector<std::shared_ptr<Page> > &Page::childs() const
{
    IMPL(const Page);
    return d->childs;
}

const std::vector<std::unique_ptr<Jgv::GLWidget::Widget>> &Page::widgets() const
{
    IMPL(const Page);
    return d->widgets;
}

bool Page::event(QEvent *event)
{
    IMPL(Page);
    if (isVisible()) {
        for (auto &widget: d->widgets) {
            if (widget->event(event)) {
                return true;
            }
        }
        // dans tous les cas, si la page est visible
        // la page capture l'évènement
        return true;
    }
    else {
        for (auto &child: d->childs) {
            if (child->event(event)) {
                return true;
            }
        }
    }
    return false;
}










