/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "blacklevelpage.h"
#include "blacklevelpage_p.h"

#include "gllabel.h"

using namespace Jgv::GLContainer;

BlackLevelPage::BlackLevelPage()
    : Page( *new BlackLevelPagePrivate)
{
    IMPL(BlackLevelPage);

    auto label = std::make_unique<GLWidget::Label>();
    label->setLines(5);
    label->setString("NIVEAU DU NOIR\n"
                     "Obstruer l'objectif, pour égaliser\n"
                     "le niveau de noir de chaque pixel.\n"
                     "E : égaliser avec l'image courante.\n"
                     "S : supprimier l'égalisation."
                     );
    label->gridRect.setSize( {6, 2} );
    label->gridRect.moveTopLeft({2,0});
    d->widgets.emplace_back( std::move(label) );

    auto button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::BlackLevelIn);
    button->gridRect.moveTopLeft({3,3});
    auto notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setBlackLevelIsActive(true);
        }
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::BlackLevelOut);
    button->gridRect.moveTopLeft({4,3});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setBlackLevelIsActive(false);
        }
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Return);
    button->gridRect.moveTopLeft({6,3});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [this, d]() {
        this->setVisible(false);
        if (d->parent != nullptr) {
            d->parent->setVisible(true);
        }
    });
    d->widgets.emplace_back( std::move(button) );




}

BlackLevelPage::~BlackLevelPage() = default;










