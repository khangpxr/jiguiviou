/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLKEYBOARD_H
#define GLKEYBOARD_H

#include "glwidget.h"

class QSize;
class QMatrix4x4;
class QEvent;
class QImage;
class QString;

namespace Jgv {

namespace GLWidget {

class KeyboardPrivate;
class Keyboard : public Widget
{

public:
    Keyboard();
    virtual ~Keyboard() override;

    const QString &string() const;
    void clearString();
    int columns() const;
    int lines() const;

    bool event(QEvent *event) override;

private:
    PIMPL(Keyboard)
}; // Keyboard

} // namespace GLWidget;

} // namespace Jgv

#endif // GLKEYBOARD_H
