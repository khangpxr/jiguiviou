/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef EXTERNSYNC_P_H
#define EXTERNSYNC_P_H

#include <cstdint>
#include <list>
#include <thread>

namespace ExternSync {

class Observer;

class SocketPrivate {
public:
    int sd = -1;
    uint32_t ip = 0xC0A80134; // 191.168.1.52
    uint16_t port = 7777;
    volatile bool run = true;
    std::unique_ptr<std::thread> threadPtr;
    std::list<std::shared_ptr<Observer>> observers;

}; // class ExternSyncPrivate

} // namespace ExternSync

#endif // EXTERNSYNC_P_H
