
find_path(PIMPL_INCLUDE_DIR NAMES pimpl.hpp)
mark_as_advanced(PIMPL_INCLUDE_DIR)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Pimpl DEFAULT_MSG PIMPL_INCLUDE_DIR)


