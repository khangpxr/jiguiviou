/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GEVIPORT_H
#define GEVIPORT_H

#include <gvcpclient.h>
#include <iport.h>


namespace Jgv {

class GevIPort : public Jgv::Gvcp::Client, public Jgv::GenICam::IPort::Interface
{
public:
    GevIPort() = default;
    virtual ~GevIPort() override = default;

    void addReceiver(uint channel, const uint32_t &address, uint16_t port);

    void read(uint8_t *pBuffer, int64_t address, int64_t length) override;
    void write(uint8_t *pBuffer, int64_t address, int64_t length) override;

}; // GevIPort

} // namespace Jgv

#endif // GEVIPORT_H
