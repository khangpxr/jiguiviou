/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "endat.h"
#include "endat_p.h"

#include <vector>
#include <cmath>
#include <arpa/inet.h>
#include <iostream>
#include <iomanip>
#include <sstream>

#ifdef ENDAT_MULTI
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/mapped_region.hpp>
using namespace boost::interprocess;
#endif


#define TCP_TIMEOUT 5000
#define EIB_AXIS_COUNT 4
#define TIMESTAMP_PERIODE 1000  // 1ms
#define TRIGGER_PERIOD 10000
#define MAX_TEXT_LEN 100
#define MAX_MAC_LENGTH 6


namespace {

int64_t encoderToUser(ENCODER_POSITION position, int64_t origin, int64_t dynamic)
{
    const long double a = static_cast<long double>(dynamic - 1) / static_cast<long double>(MAX_AXE);
    const long double pos = static_cast<long double>(position) * a;
    return std::llround(pos) + origin;
}

int64_t userToEncoder(int64_t user, int64_t origin, int64_t dynamic)
{
    const long double a = static_cast<long double>(MAX_AXE) / static_cast<long double>(dynamic - 1);
    const long double pos = static_cast<long double>(user - origin) * a;
    return std::llround(pos);
}

bool isErrCode(EIB7_ERR err)
{
    if (err == EIB7_NoError)
        return false;

    char mnemoni[40] {0};
    char message[150] {0};
    EIB7GetErrorInfo(err, mnemoni, 40, message, 150);
    std::clog << "Endat: " << mnemoni << " " << message << std::endl;
    return true;
}

void dataCallback(EIB7_HANDLE eib, unsigned long /*cnt*/, void *data)
{
    EndatPrivate *d = static_cast<EndatPrivate *>(data);
    std::vector<uint8_t> udpBuffer(200, 0);
    unsigned long entries;
    EIB7_ERR error = EIB7ReadFIFOData(eib, udpBuffer.data(), 1, &entries, 0);
    if(error == EIB7_FIFOOverflow)
    {
        //qWarning("FIFO Overflow error detected, clearing FIFO.");
        EIB7ClearFIFO(eib);
    }

    if (entries > 0)
    {
        void *field = nullptr;
        unsigned long sz = 0;

#ifdef ENDAT_MULTI
        scoped_lock<named_mutex> lock{d->sharedMutex};
        mapped_region region{d->sharedDatas, read_write};
        EndatDatas *pDatas = static_cast<EndatDatas *>(region.get_address());
#else
        EndatDatas *pDatas = &d->datas;
#endif
        if (isErrCode(EIB7GetDataFieldPtr(eib, udpBuffer.data(), EIB7_DR_Global, EIB7_PDF_TriggerCounter, &field, &sz))) {
            //qWarning("Endat: failed to get trigger counter");
        }
        //d->datas.triggerCount = *(static_cast<unsigned short *>(field));
        pDatas->triggerCount = *(static_cast<unsigned short *>(field));

        if (isErrCode(EIB7GetDataFieldPtr(eib, udpBuffer.data(), EIB7_DR_Encoder1, EIB7_PDF_Timestamp, &field, &sz))) {
            //qWarning("Endat: failed to get timestamp");
        }
        pDatas->timestamp = *(static_cast<unsigned long *>(field));

        if (isErrCode(EIB7GetDataFieldPtr(eib, udpBuffer.data(), EIB7_DR_Encoder1, EIB7_PDF_PositionData, &field, &sz))) {
            //qWarning("Endat: failed to get position 1");
        }

        // inversion du sens de rotation de la position brute du codeur
        ENCODER_POSITION encoder = d->firstSettings.inverted?ENCODER_MAX_AXE-(*(static_cast<ENCODER_POSITION *>(field))):*(static_cast<ENCODER_POSITION *>(field));
        // réduction de la résolution du codeur
        d->first = encoder >> RES_REDUCTION;

        // applique l'offset avec la référence
        ENCODER_POSITION position = d->first + d->references.firstAxe;
        // ramène la valeur du codeur au premier tour
        position %= (MAX_AXE + 1);
        pDatas->firstPosition = static_cast<int32_t>(encoderToUser(position, d->firstSettings.origin, d->firstSettings.dynamic));

        if (isErrCode(EIB7GetDataFieldPtr(eib, udpBuffer.data(), EIB7_DR_Encoder2, EIB7_PDF_PositionData, &field, &sz))) {
            //qWarning("Endat: failed to get position 2 ");
        }

        // inversion du sens de rotation de la position brute du codeur
        encoder = d->secondSettings.inverted?ENCODER_MAX_AXE-(*(static_cast<ENCODER_POSITION *>(field))):*(static_cast<ENCODER_POSITION *>(field));
        // réduction de la résolution du codeur
        d->second = encoder >> RES_REDUCTION;

        // applique l'offset avec la référence
        position = d->second + d->references.secondAxe;
        // ramène la valeur du codeur au premier tour
        position %= (MAX_AXE + 1);
        pDatas->secondPosition = static_cast<int32_t>(encoderToUser(position, d->secondSettings.origin, d->secondSettings.dynamic));

        if (isErrCode(EIB7GetDataFieldPtr(eib, udpBuffer.data(), EIB7_DR_Encoder1, EIB7_PDF_StatusWord, &field, &sz))) {
            //qWarning("Endat: failed to get status field");
        }

    }


}

std::string ipToString(uint32_t ip) {
    ip = htonl(ip);
    char saddr[INET_ADDRSTRLEN] {0};
    inet_ntop(AF_INET, &ip, saddr, INET_ADDRSTRLEN);
    return std::string(saddr);
}

} // anonymous namespace


#ifdef ENDAT_MULTI
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/mapped_region.hpp>
#endif

#ifdef ENDAT_MULTI
using namespace boost::interprocess;
#endif

Endat::Endat()
    : p_impl(new EndatPrivate)
{

#ifdef ENDAT_MULTI
    IMPL(Endat);
    d->sharedDatas.truncate(sizeof(EndatDatas));
#endif

}

Endat::~Endat()
{}

void Endat::startSoftRealTime()
{
    IMPL(Endat);

    if (d->eib != 0) {
        std::clog << "Endat: failed to start realtime, EIB allready exists" << std::endl;
        return;
    }

    char firmwareVersion[20];
    // établit la connexion TCP
    if (isErrCode(EIB7Open(d->address, &d->eib, TCP_TIMEOUT, firmwareVersion, sizeof(firmwareVersion)))) {
        std::clog << "Endat: failed to connect to " << ipToString(d->address) << std::endl;
        d->eib = 0;
        return;
    }

    // la lecture se fait sur le callback
    if (isErrCode(EIB7SetDataCallback(d->eib, d, EIB7_MD_Enable, 1, static_cast<EIB7OnDataAvailable>(&dataCallback)))) {
        //qWarning("Endat: failed to set callback");
    }

    // obtient les handles sur les axes
    EIB7_AXIS axis[EIB_AXIS_COUNT];
    unsigned long len = 0;
    EIB7GetAxis(d->eib, axis, EIB_AXIS_COUNT, &len);

    // initialise le premier axe
    if (isErrCode(EIB7InitAxis(axis[0],
                               EIB7_IT_EnDat21,
                               EIB7_EC_Rotary,
                               EIB7_RM_None,         /* not used for EnDat interface   */
                               0,                    /* not used for EnDat interface   */
                               0,                    /* not used for EnDat interface   */
                               EIB7_HS_None,
                               EIB7_LS_None,
                               EIB7_CS_CompActive,   /* not used for EnDat interface   */
                               EIB7_BW_High,         /* not used for EnDat interface   */
                               EIB7_CLK_Default,     /* default clock for EnDat 2.2    */
                               EIB7_RT_Long,         /* long recovery time             */
                               EIB7_CT_Long          /* long calculation time          */
                               ))) {
        //qWarning("Endat: failed to initialize axe 1");

    }
    // initialise le deuxième axe
    if (isErrCode(EIB7InitAxis(axis[1],
                               EIB7_IT_EnDat21,
                               EIB7_EC_Rotary,
                               EIB7_RM_None,         /* not used for EnDat interface   */
                               0,                    /* not used for EnDat interface   */
                               0,                    /* not used for EnDat interface   */
                               EIB7_HS_None,
                               EIB7_LS_None,
                               EIB7_CS_CompActive,   /* not used for EnDat interface   */
                               EIB7_BW_High,         /* not used for EnDat interface   */
                               EIB7_CLK_Default,     /* default clock for EnDat 2.2    */
                               EIB7_RT_Long,         /* long recovery time             */
                               EIB7_CT_Long          /* long calculation time          */
                               ))) {
        //qWarning("Endat: failed to initialize axe 2");

    }

    // réglage timestamp
    unsigned long timestampTicks = 0;
    if (isErrCode(EIB7GetTimestampTicks(d->eib, &timestampTicks))) {
        //qWarning("Endat: failed to get timestamp ticks");
    }
    unsigned long timestamp = TIMESTAMP_PERIODE;
    timestamp *= timestampTicks;
    if (isErrCode(EIB7SetTimestampPeriod(d->eib, timestamp))) {
        //qWarning("Endat: failed to set timestamp periode");
    }


    // activation du timestamp sur l'axe 1
    if (isErrCode(EIB7SetTimestamp(axis[0], EIB7_MD_Enable))) {
        //qWarning("Endat: failed to enable timestamp");
    }

    // data paquet
    EIB7_DataPacketSection packet[5];
    if (isErrCode(EIB7AddDataPacketSection(packet, 0, EIB7_DR_Global, EIB7_PDF_TriggerCounter))) {
        //qWarning("Endat: failed to add Global section");
    }
    if (isErrCode(EIB7AddDataPacketSection(packet, 1, EIB7_DR_Encoder1, EIB7_PDF_StatusWord | EIB7_PDF_PositionData | EIB7_PDF_Timestamp))) {
        //qWarning("Endat: failed to add encoder 1 section");
    }
    if (isErrCode(EIB7AddDataPacketSection(packet, 2, EIB7_DR_Encoder2, EIB7_PDF_StatusWord | EIB7_PDF_PositionData))) {
        //qWarning("Endat: failed to add encoder 2 section");
    }
    if (isErrCode(EIB7ConfigDataPacket(d->eib, packet, 3))) {
        //qWarning("Endat: failed to config data packet");
    }

    unsigned long ilen = 0;
    unsigned long olen = 0;
    EIB7_IO input[4];
    EIB7_IO output[4];

    if (isErrCode(EIB7GetIO(d->eib, input, 4, &ilen, output, 4, &olen))) {
        //qWarning("Endat: failed to get IO");
    }
    if (isErrCode(EIB7InitInput(input[0], EIB7_IOM_Trigger, EIB7_MD_Disable))) {
        //qWarning("Endat: failed to enable trigger");
    }
    if (isErrCode(EIB7AxisTriggerSource(axis[0], EIB7_AT_TrgInput1))) {
        //qWarning("Endat: failed to set axis trigger source");
    }
    if (isErrCode(EIB7AxisTriggerSource(axis[1], EIB7_AT_TrgInput1))) {
        //qWarning("Endat: failed to set axis trigger source");
    }


    if (isErrCode(EIB7MasterTriggerSource(d->eib, EIB7_AT_TrgInput1))) {
        //qWarning("Endat: failed to set master trigger source");
    }

    // Mode soft realtime
    if (isErrCode(EIB7SelectMode(d->eib, EIB7_OM_SoftRealtime))) {
        //qWarning("Endat: failed to set SoftRealTime");
    }

    // reset le compteur de trig
    if (isErrCode(EIB7ResetTriggerCounter(d->eib))) {
        //qWarning("Endat: failed to reset trigger counter");
    }
    // reset le compteur timestamp
    if (isErrCode(EIB7ResetTimestamp(d->eib))) {
        //qWarning("Endat: failed to reset timestamp counter");
    }

    // lance le trigger
    if (isErrCode(EIB7GlobalTriggerEnable(d->eib, EIB7_MD_Enable, EIB7_TS_TrgInput1))) {
        //qWarning("Endat: failed to enable trigger");
    }
}

uint32_t Endat::eibAddress() const
{
    IMPL(const Endat);
    return d->address;
}

void Endat::setEibAddress(uint32_t address)
{
    IMPL(Endat);
    d->address = address;
}

void Endat::stop()
{
    IMPL(Endat);

    std::clog << "Endat: stopped" << std::endl;

    if (d->eib == 0) {
        //qWarning("Endat: failed to stop EIB7, handle is null");
        return;
    }

    // coupe le trigger
    if (isErrCode(EIB7GlobalTriggerEnable(d->eib, EIB7_MD_Disable, EIB7_TS_All))) {
        //qWarning("Endat: failed to enable trigger");
    }

    // retire le callback
    if (isErrCode(EIB7SetDataCallback(d->eib, d, EIB7_MD_Disable, 1, static_cast<EIB7OnDataAvailable>(nullptr)))) {
        //qWarning("Endat: failed to set callback");
    }

    // retourne au mode polling
    if (isErrCode(EIB7SelectMode(d->eib, EIB7_OM_Polling))) {
        //qWarning("Endat: failed to set callback");
    }

    // ferme la connexion
    if (isErrCode(EIB7Close(d->eib))) {
        //qWarning("Endat: Failed to close connection");
    }
    d->eib = 0;




}

EibInfos Endat::eibInfos() const
{
    IMPL(const Endat);

    EibInfos infos { {0,0,0,0,false},0,0,"",""};

    EIB7_CONN_STATUS status = EIB7_CS_Closed;
    if (isErrCode(EIB7GetConnectionStatus(d->eib, &status))) {
        return infos;
    }

    if (status != EIB7_CS_Connected) {
        return infos;
    }

    char text[MAX_TEXT_LEN] {0};
    isErrCode(EIB7GetIdentNumber(d->eib, text, MAX_TEXT_LEN));
    infos.id = text;
    unsigned long ip {0}, netmask {0}, gateway {0};
    EIB7_MODE dhcpmode {EIB7_MD_Disable};
    isErrCode(EIB7GetNetwork(d->eib, &ip, &netmask, &gateway, &dhcpmode));
    infos.network.ip = static_cast<uint32_t>(ip);
    infos.network.netmask = static_cast<uint32_t>(netmask);
    infos.network.gateway = static_cast<uint32_t>(gateway);
    infos.network.dhcp = (dhcpmode == EIB7_MD_Enable);
    isErrCode(EIB7GetHostname(d->eib, text, MAX_TEXT_LEN));
    infos.hostname = text;
    unsigned char mac[MAX_MAC_LENGTH];
    isErrCode(EIB7GetMAC(d->eib, mac));
    infos.mac |= mac[5];
    infos.mac <<= 8;
    infos.mac |= mac[4];
    infos.mac <<= 8;
    infos.mac |= mac[3];
    infos.mac <<= 8;
    infos.mac |= mac[2];
    infos.mac <<= 8;
    infos.mac |= mac[1];
    infos.mac <<= 8;
    infos.mac |= mac[0];
    isErrCode(EIB7GetNumberOfOpenConnections(d->eib, &infos.numberOfOpenConnections));
    return infos;
}

void Endat::setNetworkConfiguration(const EibNetwork &network)
{
    IMPL(Endat);

    // ferme toutes les connexions
    if (d->eib != 0) {
        stop();
    }

    // ouvre une nouvelle connexion en mode polling
    char firmwareVersion[20];
    // établit la connexion TCP
    if (isErrCode(EIB7Open(d->address, &d->eib, TCP_TIMEOUT, firmwareVersion, sizeof(firmwareVersion)))) {
        std::clog << "Endat: failed to connect to " << ipToString(d->address) << std::endl;
        d->eib = 0;
        return;
    }
    if (isErrCode(EIB7SelectMode(d->eib, EIB7_OM_Polling))) {
        std::clog << "Endat: failed to switch mode Polling" << std::endl;
    }

    // applique les changements
    EIB7_MODE mode = network.dhcp ? EIB7_MD_Enable : EIB7_MD_Disable;
    if (isErrCode(EIB7SetNetwork(d->eib, network.ip, network.netmask, network.gateway, mode, network.dhcpTimeout))) {
        std::clog << "Endat: failed to set network parameters" << std::endl;
    }
    else {
        d->address = network.ip;
    }
}

ConnectionStatus Endat::connectionStatus() const
{
    IMPL(const Endat);

    EIB7_CONN_STATUS status = EIB7_CS_Closed;
    if (!EIB7GetConnectionStatus(d->eib, &status)) {
        switch (status) {
        case  EIB7_CS_Connected: return ConnectionStatus::Connected;
        case  EIB7_CS_Closed: return ConnectionStatus::Closed;
        case  EIB7_CS_Timeout: return ConnectionStatus::Timeout;
        case  EIB7_CS_ConnectionReset: return ConnectionStatus::Reset;
        case  EIB7_CS_TransmissionError: return ConnectionStatus::TramsmissionError;
        }
    }

    return ConnectionStatus::libeibError;
}

void Endat::startPollingMode()
{
    IMPL(Endat);

    if (d->eib != 0) {
        std::clog << "Endat polling mode: failed to start, connection exists." << std::endl;
        return;
    }


    char firmwareVersion[20];
    // établit la connexion TCP
    if (isErrCode(EIB7Open(d->address, &d->eib, TCP_TIMEOUT, firmwareVersion, sizeof(firmwareVersion)))) {
        std::clog << "Endat polling mode: failed to connect to " << ipToString(d->address) << std::endl;
        d->eib = 0;
        return;
    }


    if (isErrCode(EIB7SelectMode(d->eib, EIB7_OM_Polling))) {
        std::clog << "Endat polling mode: failed to switch mode Polling." << std::endl;
        // ferme la connexion
        if (isErrCode(EIB7Close(d->eib))) {
            std::clog << "Endat polling mode: failed to close connection." << std::endl;;
        }
        d->eib = 0;
    }
}

AxeSettings Endat::axeSettings(int axeNumber) const
{
    IMPL(const Endat);
    if (axeNumber < 1 || axeNumber > 2) {
        std::cerr << "Endat: request for out of range axe number " << axeNumber;
        return AxeSettings {false, 0, 0};
    }
    return (axeNumber == 1) ? d->firstSettings : d->secondSettings;
}

void Endat::setAxeSettings(int axeNumber, const AxeSettings &settings)
{
    IMPL(Endat);
    if (axeNumber < 1 || axeNumber > 2) {
        std::cerr << "Endat: call for out of range axe number " << axeNumber;
        return;
    }
    if (axeNumber == 1) {
        d->firstSettings = settings;
    } else {
        d->secondSettings = settings;
    }
}

AxesReference Endat::userAxesReference() const
{
    IMPL(const Endat);
    return d->userReferences;
}

void Endat::setUserAxesReference(const AxesReference &reference)
{
    IMPL(Endat);

#ifdef ENDAT_MULTI
    scoped_lock<named_mutex> lock{d->sharedMutex};
#endif
    d->userReferences = reference;
    // calcul l'écart par rapport à la postion au tour suivant
    d->references.firstAxe = (1 + MAX_AXE + userToEncoder(d->userReferences.firstAxe, d->firstSettings.origin, d->firstSettings.dynamic)) - d->first;
    d->references.secondAxe = (1 + MAX_AXE + userToEncoder(d->userReferences.secondAxe, d->secondSettings.origin, d->secondSettings.dynamic)) - d->second;
}

AxesReference Endat::axesReference() const
{
    IMPL(const Endat);
    return d->references;
}

void Endat::restoreAxesReference(const AxesReference &userReference, const AxesReference &reference)
{
    IMPL(Endat);
#ifdef ENDAT_MULTI
    scoped_lock<named_mutex> lock{d->sharedMutex};
#endif
    d->userReferences = userReference;
    d->references = reference;
}

EndatDatas Endat::datas()
{
    IMPL(Endat);
#ifdef ENDAT_MULTI
    scoped_lock<named_mutex> lock{d->sharedMutex};
    mapped_region region{d->sharedDatas, read_only};
    return *(static_cast<EndatDatas *>(region.get_address()));
#else
    return d->datas;
#endif
}


