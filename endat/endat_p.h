/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ENDAT_P_H
#define ENDAT_P_H

#include "endat.h"
#include <eib7.h>

#ifdef ENDAT_MULTI
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#endif

#ifdef ENDAT_29bits
const int RES_REDUCTION = 2;
const int64_t ENCODER_MAX_AXE = 0x7FFFFF;
#else
const int RES_REDUCTION = 8;
const int64_t ENCODER_MAX_AXE = 0x1FFFFFFF;
#endif
const int64_t MAX_AXE = ENCODER_MAX_AXE >> RES_REDUCTION;

class EndatPrivate
{
public:
    ~EndatPrivate() {
#ifdef ENDAT_MULTI
        boost::interprocess::named_mutex::remove("EndatMutex");
        boost::interprocess::shared_memory_object::remove("Endat");
#endif
    }



    uint32_t address = 0xC0A80102; // 192.168.1.2 défaut constructeur
    EIB7_HANDLE eib = 0;

    ENCODER_POSITION first;
    ENCODER_POSITION second;

    AxeSettings firstSettings {false,0,400000};
    AxeSettings secondSettings {false,0,400000};

    AxesReference references {0, 0};
    AxesReference userReferences {0, 0};

#ifdef ENDAT_MULTI
    boost::interprocess::permissions perm {666};
    boost::interprocess::shared_memory_object sharedDatas { boost::interprocess::open_or_create, "Endat", boost::interprocess::read_write,  perm};
    boost::interprocess::named_mutex sharedMutex { boost::interprocess::open_or_create, "EndatMutex"};
#else
    EndatDatas datas;
#endif

};

#endif // ENDAT_P_H
