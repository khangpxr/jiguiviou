/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PTPCLOCKDESCRIPTION_H
#define PTPCLOCKDESCRIPTION_H

#include "ptpmessages.h"

namespace PTP {

namespace ClockType
{
enum Value {
    OrdinaryClock = 0x0000,
    BoudaryClock = 0x0001,
    P2PTransparentClock = 0x0002,
    E2ETransparentClock = 0x0004,
    ManagementNode = 0x0008
};
}

namespace Management {

class ClockDescription : public ManagementTlv
{
public:
    ClockDescription();
    ClockDescription(quint8 *data);
    ClockDescription(const DerivedTypes::PtpText &physicalLayerprotocol, const QVector<quint8> &physicalAddress,
                     const DerivedTypes::PortAddress &protocolAddress, const DerivedTypes::PtpText &productDescription,
                     const DerivedTypes::PtpText &revisionData, const DerivedTypes::PtpText &userDescription);

    bool clockType(ClockType::Value type) const;

    DerivedTypes::PtpText physicalLayerProtocol() const;

    quint16 physicalAddressLength() const;

    const quint8 *physicalAddress() const;

    DerivedTypes::PortAddress protocolAddress() const;

    const quint8 *manufacturerIdentity() const;

    DerivedTypes::PtpText productDescription() const;

    DerivedTypes::PtpText revisiondata() const;

    DerivedTypes::PtpText userDescription() const;

    const quint8 *profileIdentity() const;
};


} // Management

} // Ptp


#endif // PTPCLOCKDESCRIPTION_H
