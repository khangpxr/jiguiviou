/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ptpuserdescription.h"

#include <QtEndian>

using namespace PTP::Management;

const int USERDESCRIPTION_STATIC_SIZE = 14;

UserDescription::UserDescription()
    : ManagementTlv(nearEven(USERDESCRIPTION_STATIC_SIZE + DerivedTypes::PtpText().size()))
{
    setManagementId(Id::UserDescription);
}

UserDescription::UserDescription(quint8 *data)
    : ManagementTlv(data)
{}

UserDescription::UserDescription(const PTP::DerivedTypes::PtpText &descrition)
    : ManagementTlv(nearEven(USERDESCRIPTION_STATIC_SIZE + descrition.size()))
{
    setManagementId(Id::UserDescription);
}

PTP::DerivedTypes::PtpText UserDescription::userDescription() const
{
    return DerivedTypes::PtpText(dataField());
}

