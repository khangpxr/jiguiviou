/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PTPDERIVEDTYPES_H
#define PTPDERIVEDTYPES_H

#include <QtGlobal>
#include <QScopedPointer>
#include <QVector>

namespace PTP
{

namespace ClockAccuracy {
enum Accuracy {
    Accurate25ns = 0x20,
    Accurate100ns = 0x21,
    Accurate250ns = 0x22,
    Accurate1us = 0x23,
    Accurate2_5us = 0x24,
    Accurate10us = 0x25,
    Accurate25us = 0x26,
    Accurate100us = 0x27,
    Accurate250us = 0x28,
    Accurate1ms = 0x29,
    Accurate2_5ms = 0x2A,
    Accurate10ms = 0x2B,
    Accurate25ms = 0x2C,
    Accurate100ms = 0x2D,
    Accurate250ms = 0x2E,
    Accurate1s = 0x2F,
    Accurate10s = 0x30,
    Accurate_up10s = 0x31,
    AccurateUnknow = 0xFE
};
}

namespace TlvType {
enum Value {
    Management = 0x0001,
    ManagementErrorStatus = 0x0002
};
}

namespace NetworkProtocol {
enum Value {
    UDP_IpV4 = 0x0001,
    UDP_Ipv6 = 0x0002,
    IEEE_802_3 = 0x0003,
    DeviceNet = 0x0004,
    ControlNet = 0x0005,
    Profinet = 0x0006,
    Unknow = 0xFFFE
};
}

namespace DerivedTypes
{

class Timestamp {
    quint8 * const timestamp;
public:
    Timestamp(quint8 * const memory);
    quint64 secondsField() const;
    quint32 nanosecondsField() const;
    static int size();
};

class ClockIdentity
{
    quint8 * const clockIdentity;

public:
    ClockIdentity(quint8 * const memory);

    QVector<quint8> identity() const;
    void setIdentity(const QVector<quint8> &identity);

    static int size();

};

class PortIdentity
{
    quint8 * const portIdentity;

public:
    PortIdentity(quint8 * const memory);


    ClockIdentity clockIdentity();

    quint16 portNumber() const;
    void setPortNumber(quint16 port);

    static int size();

};

class PortAddress
{
    QVector<quint8> memory;

public:
    PortAddress();
    PortAddress(quint8 *data);
    PortAddress(NetworkProtocol::Value networkProtocol, const QVector<quint8> &address);

    NetworkProtocol::Value networkProtocol() const;
    quint16 addressLength() const;
    const quint8 *addressField() const;

    int size() const;

protected:
    quint8 * const me;

};

class ClockQuality {
    quint8 * const clockQuality;
public:
    ClockQuality(quint8 * const memory);
    quint8 clockClass() const;
    quint8 clockAccuracy() const;
    quint16 offsetScaledLogVariance() const;
    static int size();
};

class Tlv
{
    QVector<quint8> memory;

public:
    Tlv(quint8 *data);
    Tlv(int memorySize);

    TlvType::Value type() const;
    quint16 lengthField() const;

    quint8 *data() const;
    int size() const;

protected:
    quint8 * const me;
};

class PtpText
{
    QVector<quint8> memory;

public:
    PtpText();
    PtpText(quint8 *data);
    PtpText(const QString &text);

    int lengthField() const;
    QString textField() const;

    const quint8 *data() const;
    int size() const;

private:
    quint8 * const me;

};

}

}


#endif // PTPDERIVEDTYPES_H
