/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LOCALMANAGER_H
#define LOCALMANAGER_H

#include <QObject>
#include <QScopedPointer>


class LocalManagerPrivate;
class LocalManager : public QObject
{
    Q_OBJECT

public:
    LocalManager(QObject *parent = nullptr);
    ~LocalManager();

    bool connectManager();
    void send();

private slots:
    void onReadyRead(int);

private:
    const QScopedPointer<LocalManagerPrivate> d;
    Q_DISABLE_COPY(LocalManager)

};


#endif // LOCALMANAGER_H
