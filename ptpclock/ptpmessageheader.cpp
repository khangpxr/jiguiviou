/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ptpmessageheader.h"

#include <QtEndian>
#include <QString>

const int HEADER_STATIC_SIZE = 34;

using namespace PTP;

MessageHeader::MessageHeader(char *data)
    : m_me(reinterpret_cast<quint8 *>(data))
{
    Q_ASSERT(m_me != 0);
}

MessageHeader::MessageHeader(int memorySize)
    : m_selfMemory(memorySize, 0),
      m_me(m_selfMemory.data())
{
    Q_ASSERT(m_me != 0);
}

MessageType::Value MessageHeader::messageType() const
{
    return static_cast<MessageType::Value>(0x0F & m_me[0]);
}

void MessageHeader::setMessageType(MessageType::Value type)
{
    m_me[0] &= 0xF0;
    m_me[0] |= static_cast<quint8>(type);
}

int MessageHeader::versionPTP() const
{
    return 0x0F & m_me[1];
}

void MessageHeader::setVersionPTP(quint8 version)
{
    m_me[1] = 0x0F & version;
}

quint16 MessageHeader::messageLength() const
{
    return qFromBigEndian<quint16>(m_me + 2);
}

void MessageHeader::setMessageLength(quint16 length)
{
    qToBigEndian<quint16>(length, m_me + 2);
}

quint8 MessageHeader::domainNumber() const
{
    return m_me[4];
}

void MessageHeader::setDomainNumber(quint8 number)
{
    m_me[4] = number;
}

bool MessageHeader::flagField(Flag::Value flag) const
{
    const quint16 flags = qFromBigEndian<quint16>(m_me + 6);
    return (flags & static_cast<quint16>(flag)) > 0;
}

void MessageHeader::setFlagField(Flag::Value flag, bool value)
{
    quint16 flags = qFromBigEndian<quint16>(m_me + 6);
    if (value) {
        flags |= static_cast<quint16>(flag);
    }
    else {
        flags &= (~static_cast<quint16>(flag));
    }
    qToBigEndian(flags, m_me+6);
}

qint64 MessageHeader::correctionField() const
{
    return qFromBigEndian<qint64>(m_me + 8);
}

void MessageHeader::setCorrectionField(qint64 value)
{
    qToBigEndian<qint64>(value, m_me + 8);
}

PTP::DerivedTypes::PortIdentity MessageHeader::sourcePortIdentity()
{
    return DerivedTypes::PortIdentity(m_me + 20);
}

quint16 MessageHeader::sequenceId() const
{
    return qFromBigEndian<quint16>(m_me + 30);
}

void MessageHeader::setSequenceId(quint16 id)
{
    qToBigEndian<quint16>(id, m_me + 30);
}

ControlField::Value MessageHeader::controlField() const
{
    return static_cast<ControlField::Value>(m_me[32]);
}

void MessageHeader::setControlField(ControlField::Value value)
{
    m_me[32] = static_cast<quint8>(value);
}

qint8 MessageHeader::logMessageInterval() const
{
    return static_cast<qint8>(m_me[33]);
}

void MessageHeader::setLogMessageInterval(qint8 interval)
{
    m_me[33] = static_cast<quint8>(interval);
}

const quint8 *MessageHeader::data() const
{
    return m_me;
}

QString MessageHeader::dump() const
{
    QString d;
    d.reserve(messageLength() * 3);
    quint8 *p = m_me;
    const quint8 *stop = m_me + messageLength();
    int i = 0;

    while (p < stop) {
        d.append(QString("%0").arg(*p, 2, 16, QLatin1Char('0')).toUpper());
        if (++i < 8) {
            d.append(" ");
        } else {
            d.append("\n");
            i = 0;
        }

        p++;
    }
    return d;
}

int MessageHeader::size()
{
    return HEADER_STATIC_SIZE;
}



