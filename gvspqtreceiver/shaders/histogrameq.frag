#version 420 core

layout (binding = 0) uniform sampler2DRect tex;
layout (binding = 1) uniform sampler1D lutTex;
layout (location = 0) out vec4 outColor;
in vec2 vTexCoord;

void main()
{
    ivec2 coord = ivec2(vTexCoord);
    float grey = texelFetch(tex, coord).r;
    float transform = texelFetch(lutTex, int(grey * 255.0), 0).r;
    outColor = vec4(transform, transform, transform, 1.0);
}
