#version 420 core
layout(binding = 0) uniform sampler2DRect tex;
layout(location = 0) out vec4 outColor;
uniform vec3 color;
uniform int var;

in vec2 vTexCoord;

void main()
{
    outColor = texture(tex, vTexCoord).rrrr * vec4(color, cos(radians(var)));
}
