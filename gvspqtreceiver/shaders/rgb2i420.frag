#version 420 core

layout(binding = 0) uniform sampler2DRect tex;
layout(location = 0) out float y;
layout(location = 1) out float u;
layout(location = 2) out float v;

in      vec2 vTexCoord;

/*
    Y =  (0.257 * rgb.r) + (0.504 * rgb.g) + (0.098 * rgb.b) + 0.0625;
    U = -(0.148 * rgb.r) - (0.291 * rgb.g) + (0.439 * rgb.b) + 0.5;
    V =  (0.439 * rgb.r) - (0.368 * rgb.g) - (0.071 * rgb.b) + 0.5;
*/
const   mat4 RGBtoYUV = mat4( 0.2570,  -0.1480,  0.4390,  0.0,
                              0.5040,  -0.2910, -0.3680,  0.0,
                              0.0980,   0.4390, -0.0710,  0.0,
                              0.0625,   0.5000,  0.5000,  1.0 );

void main()
{
    vec4 yuv = clamp(RGBtoYUV * texture(tex, vTexCoord), 0.0, 1.0);
    y = yuv.r;
    u = yuv.g;
    v = yuv.b;
}
