#version 420 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;
uniform vec2 texSize;
uniform mat4 transform;
out vec2 vTexCoord;

void main()
{
    vTexCoord = texCoord * texSize;
    gl_Position = transform * position;
}
