#version 420 core

uniform vec4 rectanglePoints;
layout(location = 0) out vec4 outColor;

in vec2 vTexCoord;

void main()
{
    if (vTexCoord.x > rectanglePoints.x && vTexCoord.x < rectanglePoints.z && vTexCoord.y > rectanglePoints.y && vTexCoord.y < rectanglePoints.w)
        discard;
    outColor = vec4 (1.0, 0.0, 0.0, 0.2);
}
