#version 420 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;
layout(std140, binding = 0) uniform shaderdatas
{
  vec2 texSize;
};
out vec4 vTexCoord;

void main()
{
    vTexCoord.xy = texCoord * texSize;
    vTexCoord.zw = vTexCoord.xx + vec2(1.0, -1.0);
    gl_Position = position;
}
