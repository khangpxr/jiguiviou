/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogramgetter.h"
#include "gvspqthistogramgetter_p.h"

#include "gvspqtpixeltransform.h"

#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLTexture>
#include <thread>

using namespace Jgv::Gvsp::QtBackend;

namespace {


void cumulativeHistogram(std::shared_ptr<uint8_t> buffer, std::size_t bufferSize, HistogramsPtr histograms)
{
    std::array<float, DISTRIBUTION_SIZE> pmf;
    pmf.fill(0.);

    // décompte des niveaux de gris
    const uint8_t *const start = buffer.get();
    for (auto p = start; p < start + bufferSize; ++p) {
        pmf[*p]++;
    }

    const float pixelsCount = static_cast<float>(bufferSize);

    // on bloque les données de l'histogramme
    QMutexLocker(&histograms->mutex);

    // normalisation directement dans le tampon partagé.
    // on profite de l'itération pour détecter le maximum des valeurs et
    // calculé l'histogramme cumulé normalisé sur [0,255]
    auto & hist = histograms->preHistogram;
    auto & max = histograms->preHistogramMaximum;
    auto & eqLut = histograms->histogramEqualizationLut;
    hist[0] = pmf[0] / pixelsCount;
    max = hist[0];
    eqLut[0] = hist[0];
    for (std::size_t i=1; i<DISTRIBUTION_SIZE; ++i) {
        hist[i] = pmf[i] / pixelsCount;
        max = (hist[i]>max)? hist[i] : max;
        eqLut[i] = eqLut[i-1] + hist[i];
    }
}

}

HistogramGetter::HistogramGetter()
    : GlEffect(*new HistogramGetterPrivate)
{}

HistogramGetter::~HistogramGetter() = default;

void HistogramGetter::initialize(QOpenGLContext *context, const GLQuadBuffers &glObject)
{
    GlEffect::initialize(context, glObject);
}

void HistogramGetter::changeGeometry(const QSize &size)
{
    IMPL(HistogramGetter);

    // la surface d'extraction est toujours centrée
    d->width = size.width() * d->relativeWidth / 100;
    d->height = size.height() * d->relativeHeight / 100;
    d->x = (size.width() - d->width) / 2;
    d->y = (size.height() - d->height) / 2;
    d->memoryPool.configure(static_cast<std::size_t>(d->width * d->height));
}

void HistogramGetter::destroy()
{}

void HistogramGetter::setHistogramPtr(HistogramsPtr ptr)
{
    IMPL(HistogramGetter);
    d->histogram.swap(ptr);
}

void HistogramGetter::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    IMPL(HistogramGetter);

    if (d->config->getHistogramIsActive()) {
        const auto sourceWidth { d->config->histogramSourceWidth() };
        const auto sourceHeight { d->config->histogramSourceHeight() };
        if (sourceWidth != d->relativeWidth || sourceHeight != d->relativeHeight) {
            d->relativeWidth = sourceWidth;
            d->relativeHeight = sourceHeight;
            changeGeometry( {metas.width, metas.height} );
        }

        auto buffer = d->memoryPool.memory();
        d->functions->glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo.fbo);
        d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, fbo.inTexture);
        d->functions->glReadBuffer(GL_COLOR_ATTACHMENT0);
        d->functions->glReadPixels(d->x, d->y, d->width, d->height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffer.get());
        d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, 0);
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // on lance le calcul en tâche de fond
        std::thread (cumulativeHistogram, buffer, d->memoryPool.memorySize(), d->histogram).detach();
    }



}
