
find_path(ENDAT_INCLUDE_DIR NAMES endat.h)
mark_as_advanced(ENDAT_INCLUDE_DIR)

find_library(ENDAT_LIBRARY NAMES endat)
mark_as_advanced(ENDAT_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Endat DEFAULT_MSG ENDAT_INCLUDE_DIR ENDAT_LIBRARY)

#set(GEVIPORT_LINK_FLAG)
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lgvcpdevices")

