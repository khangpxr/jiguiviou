/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTTIMECODEPAINTER_P_H
#define GVSPQTTIMECODEPAINTER_P_H


#include "gvspqtgleffect_p.h"

#include <QScopedPointer>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class QOpenGLShaderProgram;
class QOpenGLTexture;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

#ifdef ENDAT
const int TIMECODE_SIZE = 29;
const int TIMECODE_VARIABLES_COUNT = 22;
#else
const int TIMECODE_SIZE = 12;
const int TIMECODE_VARIABLES_COUNT = 9;
#endif
const int TIMECODE_VERTICES = TIMECODE_SIZE * 6;

class TimecodePainterPrivate final : public GlEffectPrivate
{
public:
    virtual ~TimecodePainterPrivate() final = default;

    QScopedPointer<QOpenGLShaderProgram> programPtr;
    QScopedPointer<QOpenGLTexture> texturePtr;
    QOpenGLBuffer vbo { QOpenGLBuffer::VertexBuffer};
    QOpenGLVertexArrayObject vao;
    int relativeGlyphHeight = 4;
    TimecodeAlignment timecodeAlignment = TimecodeAlignment::BottomCenter;
#ifdef ENDAT
    char timecode[TIMECODE_SIZE] {
        '0','0',':','0','0',':','0','0','.','0','0','0',
        ' ','+','0','0','0','.','0','0','0',
        ' ','0','0','0','.','0','0','0'};
    const std::array<uint, TIMECODE_VARIABLES_COUNT> TimecodeVariables {
        {0,1,3,4,6,7,9,10,11,
        13,14,15,16,18,19,20,
        22,23,24,26,27,28} };
#else
    char timecode[TIMECODE_SIZE] {
        '0','0',':','0','0',':','0','0','.','0','0','0'};
    const int TimecodeVariables[TIMECODE_VARIABLES_COUNT] {
        0,1,3,4,6,7,9,10,11};
#endif
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQTTIMECODEPAINTER_P_H
