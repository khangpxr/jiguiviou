/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogrampainter.h"
#include "gvspqthistogrampainter_p.h"
#include "gvspqtdefinitions.h"
#include "gvspqtreceiverconfig.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_4_2_Core>

using namespace Jgv::Gvsp::QtBackend;

// l'histogramme est composé de 256 colonnes, soit 512 points
const int VERTICES_COUNT = 256 * 2;
// le nombre de triangle pour dessiner l'histogramme
const int TRIANGLES_COUNT = VERTICES_COUNT - 2;
// le nombre d'indices pour dessiner tous les triangles
const int TRIANGLES_INDICES_COUNT = TRIANGLES_COUNT * 3;

HistogramPainter::HistogramPainter()
    : GlEffect(*new HistogramPainterPrivate)
{}

HistogramPainter::~HistogramPainter() = default;

void HistogramPainter::initialize(QOpenGLContext *context, const GLQuadBuffers &quad)
{
    IMPL(HistogramPainter);
    GlEffect::initialize(context, quad);

    auto vertices = [] (GLfloat *buffer) {
        for (int i=0; i<256; ++i) {
            const int x = 2 * i;
            buffer[x] = static_cast<GLfloat>(i) / 255.f;
            buffer[x + 1] = 0.5;
            buffer[x + 512] = buffer[x];
            buffer[x + 512 + 1] = 0.0;
        }

    };

    /*! Les indices vont permettre de modifier l'ordre des points
     * On groupe d'abord les points qui varient à chaque image,
     * puis les points sur l'axe des absisses (qui ne bougent jamais).
     * Cela permettra de faire des map partiels du tampon de vertices
     * et de réduire les transferts vers le serveur opengl. */
    auto indices = [] (GLushort *buffer) {
        for (GLushort i=0; i<255; i++) {
            const int x = 6 * i;
            buffer[x] = i;
            buffer[x + 1] = i + 256;
            buffer[x + 2] = i + 257;
            buffer[x + 3] = buffer[x];
            buffer[x + 4] = buffer[x + 2];
            buffer[x + 5] = i + 1;
        }
    };

    d->histogramProgramPtr.reset(new QOpenGLShaderProgram);
    d->histogramProgramPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/histogram.vert");
    d->histogramProgramPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/primitivedraw.frag");
    d->histogramProgramPtr->link();

    QMatrix4x4 mat;
    mat.setToIdentity();
    mat.translate(-0.5f, 0.5f);
    mat.scale(0.8f, 0.8f);
    mat.translate(-0.5f, 0.5f);

    d->histogramProgramPtr->bind();
    d->histogramProgramPtr->setUniformValue("color", QVector4D(0.973f, 1.0f, 0.39f, 0.5f));
    d->histogramProgramPtr->setUniformValue("transform", mat);
    d->histogramProgramPtr->release();

    GLfloat baseLineBuffer[4] {0.0, 0.0, 1.0, 0.0};
    d->baseLineVbo.create();
    d->baseLineVbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    d->baseLineVbo.bind();
    d->baseLineVbo.allocate(baseLineBuffer, 4 * sizeof(GLfloat));
    d->baseLineVbo.release();

    d->baseLineVao.create();
    d->baseLineVao.bind();
    d->baseLineVbo.bind();
    d->histogramProgramPtr->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    d->histogramProgramPtr->enableAttributeArray(0);
    d->baseLineVao.release();

    GLfloat vBuffer[VERTICES_COUNT * 2];
    vertices(vBuffer);
    d->histogramVbo.create();
    d->histogramVbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    d->histogramVbo.bind();
    d->histogramVbo.allocate(vBuffer, VERTICES_COUNT * 2 * sizeof(GLfloat));
    d->histogramVbo.release();

    GLushort iBuffer[TRIANGLES_INDICES_COUNT];
    indices(iBuffer);
    d->indicesVbo.create();
    d->indicesVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    d->indicesVbo.bind();
    d->indicesVbo.allocate(iBuffer, (TRIANGLES_INDICES_COUNT) * sizeof(GLushort));
    d->indicesVbo.release();

    d->histogramVao.create();
    d->histogramVao.bind();
    d->histogramVbo.bind();
    d->indicesVbo.bind();
    d->histogramProgramPtr->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    d->histogramProgramPtr->enableAttributeArray(0);
    d->histogramVao.release();
}

void HistogramPainter::changeGeometry(const QSize &size)
{
    Q_UNUSED(size)
}

void HistogramPainter::destroy()
{
    IMPL(HistogramPainter);

    if (d->histogramProgramPtr) {
        d->histogramProgramPtr->removeAllShaders();
    }

    if (d->baseLineVao.isCreated()) {
        d->baseLineVao.destroy();
    }

    if (d->baseLineVbo.isCreated()) {
        d->baseLineVbo.destroy();
    }
    if (d->histogramVao.isCreated()) {
        d->histogramVao.destroy();
    }

    if (d->histogramVbo.isCreated()) {
        d->histogramVbo.destroy();
    }

    if (d->indicesVbo.isCreated()) {
        d->indicesVbo.destroy();
    }

}

void HistogramPainter::setHistogramPtr(HistogramsPtr ptr)
{
    IMPL(HistogramPainter);
    d->histogram.swap(ptr);
}

void HistogramPainter::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    Q_UNUSED(metas)

    IMPL(HistogramPainter);

    if (d->config->drawHistogramIsActive()) {
        d->histogramVbo.bind();
        GLfloat *p = static_cast<GLfloat *>(d->histogramVbo.mapRange(0, 1024, QOpenGLBuffer::RangeWrite));
        d->histogram->mutex.lock();
        auto h = d->histogram->preHistogram;
        for (int i = 0; i < 256; ++i) {
            p[i*2 + 1] = h[i];
        }

        const float max = d->histogram->preHistogramMaximum;
        d->histogram->mutex.unlock();
        d->histogramVbo.unmap();
        d->histogramVbo.release();

        d->histogramProgramPtr->bind();
        d->histogramProgramPtr->setUniformValue("max", max);

        d->functions->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo.fbo);
        d->functions->glEnable(GL_BLEND);
        d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        d->functions->glBlendEquation (GL_FUNC_ADD);
        d->baseLineVao.bind();
        d->functions->glDrawArrays(GL_LINES, 0, 4);
        d->baseLineVao.release();
        d->histogramVao.bind();
        d->functions->glDrawElements(GL_TRIANGLES, TRIANGLES_INDICES_COUNT, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
        d->histogramVao.release();
        d->functions->glDisable(GL_BLEND);
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
        d->histogramProgramPtr->release();
    }
}
