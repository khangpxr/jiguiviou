/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTDEFINITIONS_H
#define GVSPQTDEFINITIONS_H

//#include "gvspdevices.h"

#include <GL/gl.h>
#include <QSharedPointer>
#include <QVector>
#include <QMutex>

class QOpenGLContext;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

using PixelFormats = QVector<quint32>;

struct ShaderDatas {
    GLfloat texSize[2];
};

class ReceiverConfig;
using ReceiverConfigPtr = QSharedPointer<ReceiverConfig>;

struct Histograms
{
    float preHistogram[256];
    float preHistogramMaximum;
//    float postHistogram[256];
//    float postHistogramMaximum;
    float histogramEqualizationLut[256];
    QMutex mutex;
};
using HistogramsPtr = QSharedPointer<Histograms>;


struct PboMetas {
    GLuint id;
    int32_t width;
    int32_t height;
    uint32_t pixelFormat;
    uint64_t timestamp;
#ifdef ENDAT
    int32_t site;
    int32_t gisement;
#endif
};

struct GLQuadBuffers {
    GLuint vbo;
    GLuint iVbo;
};

struct GLQuadRendering {
    GLuint fbo;
    GLuint inTexture;
    GLuint outTexture;
};

enum class JpegRecorderFormat
{
    Grey, Yuv
};

struct ParsedTimecode {
    int year;
    unsigned month;
    unsigned day;
    long hour;
    long minute;
    long second;
    long milli;
};


enum class TimecodeAlignment {
    TopLeft, TopCenter, TopRight,
    BottomLeft, BottomCenter, BottomRight
};

class GLVideoRenderer
{
public:
    virtual ~GLVideoRenderer() = default;
    virtual void setTextureToRender(GLuint texture) = 0;
};

} // namespace QtBackend

} // namespace Gvsp

} // namespace Jgv


#endif // GVSPQTDEFINITIONS_H
