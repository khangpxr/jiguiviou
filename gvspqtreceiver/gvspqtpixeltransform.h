/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTPIXELTRANSFORM_H
#define GVSPQTPIXELTRANSFORM_H

#include "pimpl.hpp"
#include "gvspqtdefinitions.h"


class QOpenGLContext;
class QOpenGLTexture;

namespace Jgv {

namespace Gvsp {

struct Geometry;

namespace QtBackend {


class GLVideoRenderer;
class PixelTransformPrivate;
class PixelTransform
{

protected:
    explicit PixelTransform(PixelTransformPrivate &dd);
public:
    virtual ~PixelTransform();

    static QSharedPointer<PixelTransform> makeShared(quint32 pixelFormat);
    static bool isHandledFormat(quint32 pixelFormat);
    virtual bool handlePixelFormat(quint32 pixelFormat) const;
    void setSharedContext(QOpenGLContext * context);
    void setGeneralConfig(ReceiverConfigPtr config);
    void setVideoRender(QWeakPointer<GLVideoRenderer> renderer);
    void renderPbo(const PboMetas &pbo);
    const PixelFormats &pixelFormats() const;

protected:
    BASE_PIMPL(PixelTransform)

};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv


#endif // GVSPQTPIXELTRANSFORM_H
