/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtreceiverconfig.h"
#include "gvspqtreceiverconfig_p.h"

#include <QMutexLocker>
#include <QDateTime>
#include <QDir>

using namespace Jgv::Gvsp::QtBackend;

#define LOCK QMutexLocker lock(&d->mutex)

ReceiverConfig::ReceiverConfig()
    : p_impl(new ReceiverConfigPrivate)
{
    IMPL(ReceiverConfig);
    // relocalise le path
    if (!d->path.exists("JgvVideos")) {
        d->path.mkdir("JgvVideos");
    }
    d->path.cd("JgvVideos");
}

ReceiverConfig::~ReceiverConfig() = default;

void ReceiverConfig::setJpegIsRecording(bool record)
{
    IMPL(ReceiverConfig);
    if (record && d->jpegAutoIncrement) {
        d->subDir = d->path;
        QString dirName =  QDateTime::currentDateTime().toString("yyyyMMdd_HHmmss.zzz");
        d->subDir.mkdir(dirName);
        d->subDir.cd(dirName);
    }

    d->isRecording = record;
}

bool ReceiverConfig::jpegIsRecording()
{
    IMPL(ReceiverConfig);
    return d->isRecording;
}

void ReceiverConfig::setJpegRecordingPath(const QString &path)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->path.setPath(path);
}

QString ReceiverConfig::jpegRecordingPath()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->path.path();
}

QDir ReceiverConfig::jpegEffectiveRecordingDir()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->jpegAutoIncrement?d->subDir:d->path;
}

void ReceiverConfig::setJpegIsAutoIncremental(bool autoIncrement)
{
    IMPL(ReceiverConfig);
    d->jpegAutoIncrement = autoIncrement;
}

bool ReceiverConfig::jpegIsAutoIncremental()
{
    IMPL(ReceiverConfig);
    return d->jpegAutoIncrement;
}

void ReceiverConfig::setJpegQuality(uint quality)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->jpegQuality = qBound(1u, quality, 100u);
}

uint ReceiverConfig::jpegQuality()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->jpegQuality;
}

#ifdef XMP
void ReceiverConfig::setJpegXmpMetadata(const QString &key, const QString &value)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->xmp.setMeta(key, value);
}

QString ReceiverConfig::jpegXmpMetadata(const QString &key)
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->xmp.meta(key);
}

VtoXmp ReceiverConfig::jpegXmp()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->xmp;
}
#endif

void ReceiverConfig::setOverlayTimecodeActive(bool active)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->timecodeIsActive = active;
}

bool ReceiverConfig::overlayTimecodeIsActive()
{
    IMPL(ReceiverConfig);
    return d->timecodeIsActive;
}

void ReceiverConfig::setOverlayTimecodeFontHeight(int size)
{
    IMPL(ReceiverConfig);
    d->timecodeFontHeight = qBound(1, size, 100);
}

int ReceiverConfig::overlayTimecodeFontSize()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->timecodeFontHeight;
}

void ReceiverConfig::setOverlayTimecodeAlignment(TimecodeAlignment alignment)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->timecodeAlignment = alignment;
}

TimecodeAlignment ReceiverConfig::overlayTimecodeAlignment()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->timecodeAlignment;
}

void ReceiverConfig::setOverlayReticuleIsActive(bool active)
{
    IMPL(ReceiverConfig);
    d->reticuleIsActive = active;
}

bool ReceiverConfig::overlayReticuleIsActive()
{
    IMPL(ReceiverConfig);
    return d->reticuleIsActive;
}

bool ReceiverConfig::blackLevelNeedUpdate() const
{
    IMPL(const ReceiverConfig);
    return d->blackLevelNeedUpdate;
}

void ReceiverConfig::setBlackLevelUpdated()
{
    IMPL(ReceiverConfig);
    d->blackLevelNeedUpdate = false;
}

bool ReceiverConfig::blackLevelIsActive() const
{
    IMPL(const ReceiverConfig);
    return d->blackLevelIsActive;
}

void ReceiverConfig::setBlackLevelIsActive(bool active)
{
    IMPL(ReceiverConfig);
    d->blackLevelIsActive = active;
    d->blackLevelNeedUpdate = true;
}

bool ReceiverConfig::getHistogramIsActive()
{
    IMPL(ReceiverConfig);
    return d->eqHistogram || d->drawHistogram;
}

void ReceiverConfig::setHistogramEqIsActive(bool active)
{
    IMPL(ReceiverConfig);
    d->eqHistogram = active;
}

bool ReceiverConfig::histogramEqIsActive()
{
    IMPL(ReceiverConfig);
    return d->eqHistogram;
}

void ReceiverConfig::setDrawHistogramIsActive(bool active)
{
    IMPL(ReceiverConfig);
    d->drawHistogram = active;
}

bool ReceiverConfig::drawHistogramIsActive()
{
    IMPL(ReceiverConfig);
    return d->drawHistogram;
}



void ReceiverConfig::setNonHistogramZoneIsMasked(bool masked)
{
    IMPL(ReceiverConfig);
    d->nonHistogramZoneMasked = masked;
}

bool ReceiverConfig::nonHistogramZoneIsMasked()
{
    IMPL(ReceiverConfig);
    return d->nonHistogramZoneMasked;
}

void ReceiverConfig::setHistogramSourceWidth(int width)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->histogramSourceWidth = qBound(0, width, 100);
}

int ReceiverConfig::histogramSourceWidth()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->histogramSourceWidth;
}

void ReceiverConfig::setHistogramSourceHeight(int height)
{
    IMPL(ReceiverConfig);
    LOCK;
    d->histogramSourceHeight = qBound(0, height, 100);
}

int ReceiverConfig::histogramSourceHeight()
{
    IMPL(ReceiverConfig);
    LOCK;
    return d->histogramSourceHeight;
}






