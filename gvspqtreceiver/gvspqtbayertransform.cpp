/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtbayertransform.h"
#include "gvspqtbayertransform_p.h"

#include <gvsp.h>

#include "gvspqtreticle.h"
#include "gvspqttimecodepainter.h"
#include "gvspqtjpegrecorder.h"
#include "gvspqtstateoverlay.h"

#include <QVector2D>
#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLTexture>

using namespace Jgv::Gvsp::QtBackend;

void BayerTransformPrivate::populateEffects()
{
    effects.emplace(GlEffectType::Reticle, std::make_unique<Reticle>());
    effects.emplace(GlEffectType::TimecodePainter, std::make_unique<TimecodePainter>());
    effects.emplace(GlEffectType::JpegRecorder, std::make_unique<JpegRecorder>(JpegRecorderFormat::Yuv));
    effects.emplace(GlEffectType::StateOverlay, std::make_unique<StateOverlay>());
    PixelTransformPrivate::populateEffects();
}

bool BayerTransformPrivate::initialize()
{
    if (PixelTransformPrivate::initialize()) {
        programPtr.reset( new QOpenGLShaderProgram );
        programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/bayer2rgb.vert");
        programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/bayer2rgb.frag");
        programPtr->link();
        return true;
    }
    qCritical("Bayer transform failed to initialize OpenGL resources");
    return false;
}

void BayerTransformPrivate::render(const PboMetas &pbo)
{
    context->makeCurrent(&surface);

    if (!fboPtr || (fboPtr->size() != QSize(pbo.width, pbo.height))) {
        changeGeometry({pbo.width, pbo.height});
        inTexturePtr.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
        inTexturePtr->setAutoMipMapGenerationEnabled(false);
        inTexturePtr->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
        inTexturePtr->setSize(pbo.width, pbo.height);
        inTexturePtr->setFormat(QOpenGLTexture::R8_UNorm);
        inTexturePtr->allocateStorage();
    }

    programPtr->bind();
    if (pbo.pixelFormat != currentPixelFormat) {
        auto formatToUniform = [](quint32 format) {
            switch (format) {
            case GVSP_PIX_BAYGR8: return QVector2D(1.0f, 0.0f);
            case GVSP_PIX_BAYRG8: return QVector2D(0.0f, 0.0f);
            case GVSP_PIX_BAYGB8: return QVector2D(0.0f, 1.0f);
            case GVSP_PIX_BAYBG8: return QVector2D(1.0f, 1.0f);
            default:              return QVector2D(0.0f, 0.0f);
            }
        };

        currentPixelFormat = pbo.pixelFormat;
        programPtr->setUniformValue("firstRed", formatToUniform(currentPixelFormat));
    }

    inTexturePtr->bind();
    functions->glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo.id);
    functions->glTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, pbo.width, pbo.height, GL_RED, GL_UNSIGNED_BYTE, nullptr);
    functions->glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    fboPtr->bind();
    //    functions->glClearColor(0.0, 1.0, 0.0, 0.0);
    //    functions->glClear(GL_COLOR_BUFFER_BIT);
    vao.bind();
    functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    vao.release();
    inTexturePtr->release();
    fboPtr->release();
    programPtr->release();

    GLQuadRendering quad { fboPtr->handle(), inTexturePtr->textureId(), fboPtr->texture() };
    effects[GlEffectType::Reticle]->render(quad, pbo);
    effects[GlEffectType::TimecodePainter]->render(quad, pbo);
    effects[GlEffectType::JpegRecorder]->render(quad, pbo);
    effects[GlEffectType::StateOverlay]->render(quad, pbo);

    PixelTransformPrivate::render(pbo);
    context->doneCurrent();
}

void BayerTransformPrivate::destroy()
{
    context->makeCurrent(&surface);
    if (inTexturePtr) {
        inTexturePtr->destroy();
    }
    if (programPtr) {
        programPtr->removeAllShaders();
    }
    PixelTransformPrivate::destroy();
    context->doneCurrent();
}


BayerTransform::BayerTransform()
    : PixelTransform(*new BayerTransformPrivate)
{
    IMPL(BayerTransform);
    d->pixelFormats = PixelFormats {GVSP_PIX_BAYGR8,GVSP_PIX_BAYRG8,GVSP_PIX_BAYGB8,GVSP_PIX_BAYBG8};
}

BayerTransform::~BayerTransform()
{}


