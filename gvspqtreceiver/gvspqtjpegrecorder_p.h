/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTJPEGRECORDER2_P_H
#define GVSPQTJPEGRECORDER2_P_H

#include "gvspqtgleffect_p.h"
#include "gvspqtmemorypool.h"

#include <QOpenGLVertexArrayObject>

class QOpenGLShaderProgram;
class QOpenGLFramebufferObject;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

class JpegRecorderPrivate final : public GlEffectPrivate
{
    using Render = void (JpegRecorderPrivate::*) (const GLQuadRendering &, const PboMetas &);
public:
    JpegRecorderPrivate(JpegRecorderFormat format);
    virtual ~JpegRecorderPrivate() override final;

    const JpegRecorderFormat format;
    MemoryPool pool;

    // pour yuv
    QScopedPointer<QOpenGLShaderProgram> programPtr;
    QScopedPointer<QOpenGLShaderProgram> scaledProgramPtr;
    QOpenGLVertexArrayObject vao;
    QScopedPointer<QOpenGLFramebufferObject> fboPtr;
    MemoryPool uvPool;
    GLuint inTexture;

    Render render;
    void renderAsGrey(const GLQuadRendering &quad, const PboMetas &metas);
    void renderAsYuv(const GLQuadRendering &quad, const PboMetas &metas);

};


} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQTJPEGRECORDER2_P_H
