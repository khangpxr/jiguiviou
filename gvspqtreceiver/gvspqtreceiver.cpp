/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtreceiver.h"
//#include "gvspqtreceiver_p.h"

#include "gvspqtopenglpipeline.h"


#include <QObject>

using namespace Jgv::Gvsp::QtBackend;


//ReceiverPrivate::ReceiverPrivate(Jgv::Gvsp::MemoryAllocator *allocator)
//    : Gvsp::ReceiverPrivate(allocator)
//{}


Receiver::Receiver()
    : Gvsp::Receiver(/**new ReceiverPrivate*/(new OpenGLPipeline))
{
    // permet l'accès aux shaders en tant que ressource
    initMyResource();
}

Receiver::~Receiver()
{}

void Receiver::setSharedContext(QOpenGLContext *context)
{
    static_cast<OpenGLPipeline &>(allocator()).setSharedContext(context);
}

void Receiver::setGLVideoRenderer(QWeakPointer<GLVideoRenderer> renderer)
{
    static_cast<OpenGLPipeline &>(allocator()).setVideoRender(renderer);
}

QWeakPointer<ReceiverConfig> Receiver::configPtr() const
{
    return static_cast<const OpenGLPipeline &>(allocator()).config();
}




