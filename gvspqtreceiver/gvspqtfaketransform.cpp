/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtfaketransform.h"
#include "gvspqtfaketransform_p.h"

#include "gvsp.h"

#include "gvspqttimecodepainter.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions_4_2_Core>

#include <QImage>
#include <QPainter>
#include <QBrush>

using namespace Jgv::Gvsp::QtBackend;

void FakeTransformPrivate::populateEffects()
{
    effects.emplace(GlEffectType::TimecodePainter, std::make_unique<TimecodePainter>());
    PixelTransformPrivate::populateEffects();
}

bool FakeTransformPrivate::initialize()
{
    if (PixelTransformPrivate::initialize()) {
        programPtr.reset( new QOpenGLShaderProgram );
        programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
        programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/unicolortexture.frag");
        programPtr->link();
        programPtr->bind();
        programPtr->setUniformValue("color", QVector4D(1.0, 1.0, 1.0, 1.0));
        programPtr->release();
        return true;
    }
    qCritical("Fake transform failed to initialize OpenGL resources");
    return false;
}

void FakeTransformPrivate::render(const PboMetas &pbo)
{
    context->makeCurrent(&surface);
    if (!fboPtr || (fboPtr->size() != QSize(pbo.width, pbo.height))) {
        changeGeometry({pbo.width, pbo.height});

        QImage image(pbo.width, pbo.height, QImage::Format_Grayscale8);
        image.fill(0);
        QPainter painter(&image);
        painter.setPen(Qt::white);
        painter.setFont(QFont("monospace", pbo.height / 15));
        painter.drawText(QRectF(0,0,pbo.width, pbo.height),
                         Qt::AlignCenter, QObject::trUtf8("Ce format de pixels\nn'est pas pris\nen charge !").toUpper());

        inTexturePtr.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
        inTexturePtr->setAutoMipMapGenerationEnabled(false);
        inTexturePtr->setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
        inTexturePtr->setSize(pbo.width, pbo.height);
        inTexturePtr->setFormat(QOpenGLTexture::R8_UNorm);
        inTexturePtr->allocateStorage();

        inTexturePtr->bind();
        inTexturePtr->setData(QOpenGLTexture::Red, QOpenGLTexture::UInt8, image.bits());

    }
    functions->glActiveTexture(GL_TEXTURE0);
    inTexturePtr->bind();

    // on fait un premier rendu de la texture brute
    programPtr->bind();
    fboPtr->bind();
    //functions->glViewport(0, 0, pbo.width, pbo.height);
    functions->glClearColor(0.0, 1.0, 0.0, 0.0);
    functions->glClear(GL_COLOR_BUFFER_BIT);
    vao.bind();
    functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    vao.release();
    programPtr->release();
    inTexturePtr->release();

    GLQuadRendering quad { fboPtr->handle(), inTexturePtr->textureId(), fboPtr->texture() };
    effects[GlEffectType::TimecodePainter]->render(quad, pbo);
    fboPtr->release();

    PixelTransformPrivate::render(pbo);
    context->doneCurrent();
}

void FakeTransformPrivate::destroy()
{
    context->makeCurrent(&surface);
    if (inTexturePtr) {
        inTexturePtr->destroy();
    }
    if (programPtr) {
        programPtr->removeAllShaders();
    }
    PixelTransformPrivate::destroy();
    context->doneCurrent();
}

FakeTransform::FakeTransform()
    : PixelTransform(*new FakeTransformPrivate)
{
    IMPL(FakeTransform);
    d->pixelFormats = PixelFormats {GVSP_PIX_YUV411_PACKED};
}

FakeTransform::~FakeTransform()
{}

bool FakeTransform::handlePixelFormat(quint32 pixelFormat) const
{
    return ! PixelTransform::isHandledFormat(pixelFormat);
}




