/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqttimecodepainter.h"
#include "gvspqttimecodepainter_p.h"


#include "gvspqtpixeltransform.h"
#include "date-master/date.h"

#include <QImage>
#include <QFont>
#include <QFontMetricsF>
#include <QPainter>

#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLFunctions_4_2_Core>

using namespace Jgv::Gvsp::QtBackend;

struct Point {
    GLfloat x;
    GLfloat y;
};

struct Vertex {
    Point position;
    Point coordinate;
};

namespace {

void updateTimecode(quint64 timestamp, char * const timecode)
{
    std::chrono::system_clock::time_point tp;
    tp+= std::chrono::nanoseconds(timestamp);

    using namespace date;

    auto const dp = floor<days>(tp);
    auto left = tp-dp;
    auto const hour = std::chrono::duration_cast<std::chrono::hours>(left);
    left-=hour;
    auto const minute = std::chrono::duration_cast<std::chrono::minutes>(left);
    left-=minute;
    auto const second = std::chrono::duration_cast<std::chrono::seconds>(left);
    left-=second;
    auto const milli = std::chrono::duration_cast<std::chrono::milliseconds>(left);

    timecode[0] = static_cast<char>(48 + hour.count()/10);
    timecode[1] = static_cast<char>(48 + hour.count()%10);
    timecode[3] = static_cast<char>(48 + minute.count()/10);
    timecode[4] = static_cast<char>(48 + minute.count()%10);
    timecode[6] = static_cast<char>(48 + second.count()/10);
    timecode[7] = static_cast<char>(48 + second.count()%10);
    timecode[9] = static_cast<char>(48 + milli.count()/100);
    const int mod = milli.count()%100;
    timecode[10] = static_cast<char>(48 + mod/10);
    timecode[11] = static_cast<char>(48 + mod%10);
}

#ifdef ENDAT
void signToChar(int signedInt, char * const value)
{
    value[0] = signedInt<0?'-':'+';
}

const char ZERO_CHAR = 0x30;
void milligradeToCharGrade(int milligrade, char * const grade)
{
    int s = std::abs(milligrade);
    grade[0] = ZERO_CHAR + static_cast<int8_t>(s/100000);
    s = s % 100000;
    grade[1] = ZERO_CHAR + static_cast<int8_t>(s/10000);
    s = s % 10000;
    grade[2] = ZERO_CHAR + static_cast<int8_t>(s/1000);
    s = s % 1000;
    grade[4] = ZERO_CHAR + static_cast<int8_t>(s/100);
    s = s % 100;
    grade[5] = ZERO_CHAR + static_cast<int8_t>(s/10);
    s = s % 10;
    grade[6] = ZERO_CHAR + static_cast<int8_t>(s);
}

inline void updateSite(int site, char *const timecode)
{
    signToChar(site, timecode + 13);
    milligradeToCharGrade(site, timecode + 14);
}

inline void updateGisement(int gisement, char * const timecode)
{
    milligradeToCharGrade(gisement, timecode + 22);
}
#endif

QImage glyphsImage(int glyphHeight)
{
    QFont font ("monospace", glyphHeight);
    QFontMetrics fm(font);
    const int pixelsWide = fm.width('0');
    const int pixelsHigh = fm.height();

    QImage image( pixelsWide * 5, pixelsHigh * 3, QImage::Format_Grayscale8);
    image.fill(0);
    QPainter painter(&image);
    painter.setFont(font);
    painter.setPen(Qt::white);
    painter.setRenderHint(QPainter::TextAntialiasing);
    QRect rect (0, 0, pixelsWide, pixelsHigh);
    painter.drawText(rect, Qt::AlignCenter, "0");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "1");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "2");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "3");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "4");
    rect.translate(0, pixelsHigh);
    painter.drawText(rect, Qt::AlignCenter, "9");
    rect.translate(-pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "8");
    rect.translate(-pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "7");
    rect.translate(-pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "6");
    rect.translate(-pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "5");
    rect.translate(0, pixelsHigh);
    painter.drawText(rect, Qt::AlignCenter, "-");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, "+");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, ".");
    rect.translate(pixelsWide, 0);
    painter.drawText(rect, Qt::AlignCenter, ":");

    return image;
}

std::array<Vertex, TIMECODE_VERTICES> timecodeVertices()
{
    std::array<Vertex, TIMECODE_VERTICES> array;
    const float dx = 2.0f/TIMECODE_SIZE;
    const float dcx = 1.0f / 5.0f;
    const float dcy = 1.0f / 3.0f;
    array[0] = {{     -1.0f,-1.0f},{0.0f, 0.0f}};
    array[1] = {{-1.0f + dx, 1.0f},{ dcx,  dcy}};
    array[2] = {{     -1.0f, 1.0f},{0.0f,  dcy}};
    array[3] = {{     -1.0f,-1.0f},{0.0f, 0.0f}};
    array[4] = {{-1.0f + dx,-1.0f},{ dcx, 0.0f}};
    array[5] = {{-1.0f + dx, 1.0f},{ dcx,  dcy}};
    for (uint i=6; i<array.size(); i+=6) {
        array[  i] = {{array[i-6].position.x + dx,-1.0f},{0.0f, 0.0f}};
        array[i+1] = {{array[i-5].position.x + dx, 1.0f},{ dcx,  dcy}};
        array[i+2] = {{array[i-4].position.x + dx, 1.0f},{0.0f,  dcy}};
        array[i+3] = {{array[i-3].position.x + dx,-1.0f},{0.0f, 0.0f}};
        array[i+4] = {{array[i-2].position.x + dx,-1.0f},{ dcx, 0.0f}};
        array[i+5] = {{array[i-1].position.x + dx, 1.0f},{ dcx,  dcy}};
    }
    return array;
};

#define m00 {     0.0f, 0.0f}
#define m10 {1.0f/5.0f, 0.0f}
#define m20 {2.0f/5.0f, 0.0f}
#define m30 {3.0f/5.0f, 0.0f}
#define m40 {4.0f/5.0f, 0.0f}
#define m50 {5.0f/5.0f, 0.0f}

#define m01 {     0.0f, 1.0f/3.0f}
#define m11 {1.0f/5.0f, 1.0f/3.0f}
#define m21 {2.0f/5.0f, 1.0f/3.0f}
#define m31 {3.0f/5.0f, 1.0f/3.0f}
#define m41 {4.0f/5.0f, 1.0f/3.0f}
#define m51 {     1.0, 1.0f/3.0f}

#define m02 {     0.0f, 2.0f/3.0f}
#define m12 {1.0f/5.0f, 2.0f/3.0f}
#define m22 {2.0f/5.0f, 2.0f/3.0f}
#define m32 {3.0f/5.0f, 2.0f/3.0f}
#define m42 {4.0f/5.0f, 2.0f/3.0f}
#define m52 {     1.0, 2.0f/3.0f}

#define m03 {     0.0f, 1.0f}
#define m13 {1.0f/5.0f, 1.0f}
#define m23 {2.0f/5.0f, 1.0f}
#define m33 {3.0f/5.0f, 1.0f}
#define m43 {4.0f/5.0f, 1.0f}
#define m53 {     1.0f, 1.0f}

std::vector<Point> coordinates(char c)
{
    switch (c) {
    case '0':
        return {m00,m11,m01,m00,m10,m11};
    case '1':
        return {m10,m21,m11,m10,m20,m21};
    case '2':
        return {m20,m31,m21,m20,m30,m31};
    case '3':
        return {m30,m41,m31,m30,m40,m41};
    case '4':
        return {m40,m51,m41,m40,m50,m51};
    case '5':
        return {m01,m12,m02,m01,m11,m12};
    case '6':
        return {m11,m22,m12,m11,m21,m22};
    case '7':
        return {m21,m32,m22,m21,m31,m32};
    case '8':
        return {m31,m42,m32,m31,m41,m42};
    case '9':
        return {m41,m52,m42,m41,m51,m52};
    case '-':
        return {m02,m13,m03,m02,m12,m13};
    case '+':
        return {m12,m23,m13,m12,m22,m23};
    case '.':
        return {m22,m33,m23,m22,m32,m33};
    case ':':
        return {m32,m43,m33,m32,m42,m43};
    default:
        return {m42,m53,m43,m42,m52,m53};
    }
};

QMatrix4x4 transformMatrix(const QSizeF &imageSize, const QSizeF &textureSize, float relativeGlyphHeight, TimecodeAlignment alignment)
{
    // normalisation de la taille du timecode
    const auto glyphsWidth = TIMECODE_SIZE * 2.0 * textureSize.width() / 5.0 / imageSize.width();
    const auto glyphHeight = 2.0 * textureSize.height() / 3.0 / imageSize.height();
    const float ratio = static_cast<float>(glyphsWidth / glyphHeight);
    // la matrice de transformation du quad
    QMatrix4x4 transform;
    transform.setToIdentity();
    switch (alignment) {
    case TimecodeAlignment::TopLeft:
        transform.translate(-1.0f + relativeGlyphHeight * ratio, -1.0f +relativeGlyphHeight);
        break;
    case TimecodeAlignment::TopCenter:
        transform.translate(0.0f, -1.0f +relativeGlyphHeight);
        break;
    case TimecodeAlignment::TopRight:
        transform.translate(1.0f -relativeGlyphHeight * ratio, -1.0f +relativeGlyphHeight);
        break;
    case TimecodeAlignment::BottomLeft:
        transform.translate(-1.0f + relativeGlyphHeight * ratio, 1.0f -relativeGlyphHeight);
        break;
    case TimecodeAlignment::BottomCenter:
        transform.translate(0.0f, 1.0f -relativeGlyphHeight);
        break;
    case TimecodeAlignment::BottomRight:
        transform.translate(1.0f -relativeGlyphHeight * ratio, 1.0f -relativeGlyphHeight);
        break;
    }
    transform.scale(ratio, 1.0);
    transform.scale(relativeGlyphHeight);
    return transform;
}

}

TimecodePainter::TimecodePainter()
    : GlEffect(*new TimecodePainterPrivate)
{}

TimecodePainter::~TimecodePainter() = default;

void TimecodePainter::initialize(QOpenGLContext *context, const GLQuadBuffers &quad)
{
    IMPL(TimecodePainter);
    GlEffect::initialize(context, quad);

    d->programPtr.reset(new QOpenGLShaderProgram);
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/scaledtexture.vert");
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/unicolortexture.frag");
    d->programPtr->link();
    d->programPtr->bind();
    d->programPtr->setUniformValue("color", QVector4D(1.0, 1.0, 1.0, 1.0));
    d->programPtr->release();

    auto vertices {timecodeVertices()};
    // fixe les invariables
    auto glyph = coordinates(':');
    for (uint i=0; i<6; ++i) {
        vertices[i + 2 * 6].coordinate = glyph[i];
        vertices[i + 5 * 6].coordinate = glyph[i];
    }
    glyph = coordinates('.');
    for (uint i=0; i<6; ++i) {
        vertices[i + 8 * 6].coordinate = glyph[i];
    }
#ifdef ENDAT
    for (uint i=0; i<6; ++i) {
        vertices[i + 17 * 6].coordinate = glyph[i];
        vertices[i + 25 * 6].coordinate = glyph[i];
    }

    glyph = coordinates(' ');
    for (uint i=0; i<6; ++i) {
        vertices[i + 12 * 6].coordinate = glyph[i];
        vertices[i + 21 * 6].coordinate = glyph[i];
    }
#endif

    d->vbo.create();
    d->vbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    d->vbo.bind();
    d->vbo.allocate(vertices.data(), vertices.size() * static_cast<int>(sizeof(Vertex)));
    d->vbo.release();

    d->vao.create();
    d->vao.bind();
    d->vbo.bind();
    d->functions->glEnableVertexAttribArray(0);
    d->functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0 + offsetof(Vertex, position)));
    d->functions->glEnableVertexAttribArray(1);
    d->functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0 + offsetof(Vertex, coordinate)));
    d->vao.release();


}

void TimecodePainter::changeGeometry(const QSize &size)
{
    IMPL(TimecodePainter);


    // contruction de l'image des glyphs
    auto image = glyphsImage(30);

    // suppression de la texture actuelle
    if (d->texturePtr) {
        d->texturePtr->destroy();
    }
    // construction de la nouvelle texture
    d->texturePtr.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
    d->texturePtr->setAutoMipMapGenerationEnabled(false);
    d->texturePtr->setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
    d->texturePtr->setSize(image.width(), image.height());
    d->texturePtr->setFormat(QOpenGLTexture::R8_UNorm);
    d->texturePtr->allocateStorage();
    d->texturePtr->bind();
    d->texturePtr->setData(QOpenGLTexture::Red, QOpenGLTexture::UInt8, image.constBits());
    d->texturePtr->release();

    d->programPtr->bind();
    d->programPtr->setUniformValue("texSize", QVector2D(image.width(), image.height()));
    d->programPtr->setUniformValue("transform", transformMatrix( size, image.size(), d->relativeGlyphHeight / 100.f, d->timecodeAlignment));
    d->programPtr->release();

}

void TimecodePainter::destroy()
{
    IMPL(TimecodePainter);

    if (d->programPtr) {
        d->programPtr->removeAllShaders();
    }

    if (d->texturePtr) {
        d->texturePtr->destroy();
    }

    if (d->vbo.isCreated()) {
        d->vbo.destroy();
    }

    if (d->vao.isCreated()) {
        d->vao.destroy();
    }
}

void TimecodePainter::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    Q_UNUSED(fbo)

    IMPL(TimecodePainter);

    if (d->config->overlayTimecodeIsActive()) {
        // teste si quelque chose a changé
        if (d->config->overlayTimecodeFontSize() != d->relativeGlyphHeight || d->config->overlayTimecodeAlignment() != d->timecodeAlignment) {
            d->relativeGlyphHeight = d->config->overlayTimecodeFontSize();
            d->timecodeAlignment = d->config->overlayTimecodeAlignment();

            const QSizeF imageSize {static_cast<qreal>(metas.width), static_cast<qreal>(metas.height)};
            const QSizeF textureSize {static_cast<qreal>(d->texturePtr->width()), static_cast<qreal>(d->texturePtr->height())};

            d->programPtr->bind();
            d->programPtr->setUniformValue("transform", transformMatrix( imageSize, textureSize, d->relativeGlyphHeight / 100.f, d->timecodeAlignment));
            d->programPtr->release();

        }

        auto mapGlyph = [d](Point *p, uint index) {
            const auto glyph = coordinates(d->timecode[index]);
            const uint offset = index * 12;
            p[offset + 1] = glyph[0];
            p[offset + 3] = glyph[1];
            p[offset + 5] = glyph[2];
            p[offset + 7] = glyph[3];
            p[offset + 9] = glyph[4];
            p[offset + 11] = glyph[5];
        };

        updateTimecode(metas.timestamp, d->timecode);
    #ifdef ENDAT
        updateSite(metas.site, d->timecode);
        updateGisement(metas.gisement, d->timecode);
    #endif
        d->vbo.bind();
        Point *p = reinterpret_cast<Point *>(d->vbo.map(QOpenGLBuffer::WriteOnly));
        for (const auto &varIndex: d->TimecodeVariables) {
            mapGlyph(p, varIndex);
        }

        d->vbo.unmap();
        d->vbo.release();

        d->programPtr->bind();
        d->texturePtr->bind();
        d->functions->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo.fbo);
        d->functions->glEnable(GL_BLEND);
        d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        d->functions->glBlendEquation (GL_FUNC_ADD);
        d->vao.bind();
        d->functions->glDrawArrays(GL_TRIANGLES, 0, TIMECODE_VERTICES);
        d->vao.release();
        d->functions->glDisable(GL_BLEND);
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
        d->texturePtr->release();
        d->programPtr->release();
    }
}
