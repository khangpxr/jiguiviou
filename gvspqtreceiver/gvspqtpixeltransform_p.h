/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTPIXELTRANSFORM_P_H
#define GVSPQTPIXELTRANSFORM_P_H

#include "gvspqtdefinitions.h"
//#include "gvspdevices.h"
//#include "gvspqthistogramgetter.h"
//#include "gvspqthistogrameequalization.h"
//#include "gvspqtrectanglemask.h"
//#include "gvspqthistogrampainter.h"
//#include "gvspqtreticle.h"
//#include "gvspqtjpegrecorder.h"
//#include "gvspqtstateoverlay.h"
//#include "gvspqttimecodepainter.h"

#include <vector>
#include <memory>

#include <QVector>
#include <QQueue>
#include <QMutex>
#include <QWaitCondition>
#include <QPair>
#include <QScopedPointer>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions_4_2_Core>

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>

class QOpenGLFramebufferObject;
class QOpenGLShaderProgram;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

enum class GlEffectType {
    HistogramGetter,
    HistogramEqualization,
    RectangleMask,
    HistogramPainter,
    Reticle,
    JpegRecorder,
    StateOverlay,
    TimecodePainter
};

class GlEffect;
using GlEffectPtr = std::unique_ptr<GlEffect>;
class GLVideoRenderer;

class PixelTransformPrivate
{
public:
    virtual ~PixelTransformPrivate() = default;

    QOpenGLContext * sharedContext = nullptr;
    QVector<quint32> pixelFormats {0};
    std::map<GlEffectType, GlEffectPtr> effects;
    QScopedPointer<QOpenGLContext> context;
    QOffscreenSurface surface;
    QOpenGLFunctions_4_2_Core *functions = nullptr;
    QOpenGLBuffer vbo { QOpenGLBuffer::VertexBuffer};
    QOpenGLBuffer indicesVbo { QOpenGLBuffer::IndexBuffer };
    QOpenGLVertexArrayObject vao;
    GLuint gbo = 0;
    QScopedPointer<QOpenGLFramebufferObject> fboPtr;
    QScopedPointer<QOpenGLFramebufferObject> sharedFboPtr;
    QScopedPointer<QOpenGLShaderProgram> sharedFboProgramPtr;
    ReceiverConfigPtr config;
    QWeakPointer<GLVideoRenderer> renderer;
    HistogramsPtr histograms { new Histograms };

    std::thread renderThread;
    std::mutex renderMutex;
    std::condition_variable renderCondition;
    std::mutex queueMutex;
    std::queue<PboMetas> pboQueue;

    volatile bool running { true };
    void loop();
    virtual void populateEffects();
    virtual bool initialize();
    virtual void render(const PboMetas &pbo);
    virtual void changeGeometry(const QSize &size);
    virtual void destroy();
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQTPIXELTRANSFORM_P_H
