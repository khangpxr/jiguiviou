/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogrameequalization.h"
#include "gvspqthistogrameequalization_p.h"

#include "gvspqtdefinitions.h"
#include "gvspqtreceiverconfig.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLFunctions_4_2_Core>

using namespace Jgv::Gvsp::QtBackend;

HistogramEqualization::HistogramEqualization()
    : GlEffect(*new HistogramEqualizationPrivate)
{}

HistogramEqualization::~HistogramEqualization() = default;

void HistogramEqualization::initialize(QOpenGLContext *context, const GLQuadBuffers &glObject)
{
    IMPL(HistogramEqualization);
    GlEffect::initialize(context, glObject);

    d->programPtr.reset(new QOpenGLShaderProgram);
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/histogrameq.frag");
    d->programPtr->link();

    d->lutTexture.reset( new QOpenGLTexture(QOpenGLTexture::Target1D) );
    d->lutTexture->setAutoMipMapGenerationEnabled(false);
    d->lutTexture->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
    d->lutTexture->setSize(LUT_SIZE);
    d->lutTexture->setFormat(QOpenGLTexture::R32F);
    d->lutTexture->allocateStorage();

    d->vao.create();
    d->vao.bind();
    d->functions->glBindBuffer(GL_ARRAY_BUFFER, glObject.vbo);
    d->functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0));
    d->functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(2 * sizeof (GLfloat)));
    d->functions->glEnableVertexAttribArray(0);
    d->functions->glEnableVertexAttribArray(1);
    d->functions->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glObject.iVbo);
    d->vao.release();
}

void HistogramEqualization::changeGeometry(const QSize &/*size*/)
{}

void HistogramEqualization::destroy()
{
    IMPL(HistogramEqualization);

    if (d->lutTexture) {
        d->lutTexture->destroy();
    }

    if (d->programPtr) {
        d->programPtr->removeAllShaders();
    }

    if (d->vao.isCreated()) {
        d->vao.destroy();
    }
}

void HistogramEqualization::setHistogramPtr(HistogramsPtr ptr)
{
    IMPL(HistogramEqualization);
    d->histogram.swap(ptr);
}

void HistogramEqualization::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    Q_UNUSED(fbo)
    Q_UNUSED(metas)

    IMPL(HistogramEqualization);

    if (!d->config) {
        qWarning("HistogramEqualization failed to render, no general config access");
        return;
    }

    if (d->config->histogramEqIsActive()) {
        d->programPtr->bind();
        d->functions->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo.fbo);
        d->functions->glActiveTexture(GL_TEXTURE1);
        d->lutTexture->bind();

        // on bloque l'accès au lut
        QMutexLocker locker(&d->histogram->mutex);
        d->lutTexture->setData(QOpenGLTexture::Red, QOpenGLTexture::Float32, d->histogram->histogramEqualizationLut, nullptr);

        d->functions->glActiveTexture(GL_TEXTURE0);
        d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, fbo.inTexture);
        d->vao.bind();
        d->functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
        d->lutTexture->release();
        d->functions->glBindTexture(GL_TEXTURE_RECTANGLE, 0);
        d->vao.release();
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
        d->programPtr->release();
    }

}
