/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtgleffect.h"
#include "gvspqtgleffect_p.h"

#include <QOpenGLContext>
#include <QOpenGLFunctions_4_2_Core>


using namespace Jgv::Gvsp::QtBackend;

GlEffect::GlEffect(GlEffectPrivate &dd)
    : p_impl(&dd)
{}

GlEffect::~GlEffect()
{}

void GlEffect::initialize(QOpenGLContext *context, const GLQuadBuffers &quad)
{
    Q_UNUSED(quad)
    IMPL(GlEffect);
    d->functions = context->versionFunctions<QOpenGLFunctions_4_2_Core>();
}

void GlEffect::setGeneralConfig(ReceiverConfigPtr config)
{
    IMPL(GlEffect);
    d->config.swap(config);
}


