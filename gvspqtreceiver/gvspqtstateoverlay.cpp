/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtstateoverlay.h"
#include "gvspqtstateoverlay_p.h"
#include "gvspqtdefinitions.h"
#include "gvspqtreceiverconfig.h"

#include <QImage>
#include <QPainter>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLTexture>

using namespace Jgv::Gvsp::QtBackend;

StateOverlay::StateOverlay()
    : GlEffect(*new StateOverlayPrivate)
{}

StateOverlay::~StateOverlay() = default;

void StateOverlay::initialize(QOpenGLContext *context, const GLQuadBuffers &glObject)
{
    IMPL(StateOverlay);
    GlEffect::initialize(context, glObject);

    d->programPtr.reset(new QOpenGLShaderProgram);
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/scaledtexture.vert");
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/unicolortexturevaralpha.frag");
    d->programPtr->link();

    d->programPtr->bind();
    d->programPtr->setUniformValue("color", QVector3D(1.0, 0.0, 0.0));
    d->programPtr->release();

    d->varID = d->programPtr->uniformLocation("var");

    d->vao.create();
    d->vao.bind();
    d->functions->glBindBuffer(GL_ARRAY_BUFFER, glObject.vbo);
    d->functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0));
    d->functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(2 * sizeof (GLfloat)));
    d->functions->glEnableVertexAttribArray(0);
    d->functions->glEnableVertexAttribArray(1);
    d->functions->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glObject.iVbo);
    d->vao.release();

    d->timer.start();
}

void StateOverlay::changeGeometry(const QSize &size)
{
    IMPL(StateOverlay);

    // on réduit la texture à 5 %
    const QSize texSize(size * 5 / 100);
    const QRectF drawRect {QPointF(1.0, 1.0), QSizeF(texSize) - QSizeF(1.0, 1.0)};
    // création de l'image
    QImage image { texSize.width(), texSize.height(), QImage::Format_Grayscale8 };
    image.fill(0);
    QPainter painter(&image);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    painter.setBrush(QBrush(Qt::white));
    painter.drawEllipse(drawRect);

    // suppression de la texture actuelle
    if (d->texturePtr) {
        d->texturePtr->destroy();
    }
    // construction de la nouvelle texture
    d->texturePtr.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
    d->texturePtr->setAutoMipMapGenerationEnabled(false);
    d->texturePtr->setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
    d->texturePtr->setSize(image.width(), image.height());
    d->texturePtr->setFormat(QOpenGLTexture::R8_UNorm);
    d->texturePtr->allocateStorage();
    d->texturePtr->bind();
    d->texturePtr->setData(QOpenGLTexture::Red, QOpenGLTexture::UInt8, image.constBits());
    d->texturePtr->release();

    // la matrice de transformation du quad
    const float ratio = static_cast<float>(size.width()) / static_cast<float>(size.height());
    QMatrix4x4 transform;
    transform.setToIdentity();
    // en haut à gauche
    transform.translate(-0.9f, -0.9f);
    // réduction à 5 %
    transform.scale(0.05f, 0.05f * ratio);

    d->programPtr->bind();
    d->programPtr->setUniformValue("texSize", QVector2D(texSize.width(), texSize.height()));
    d->programPtr->setUniformValue("transform", transform);
    d->programPtr->release();

}

void StateOverlay::destroy()
{
    IMPL(StateOverlay);

    if (d->programPtr) {
        d->programPtr->removeAllShaders();
    }

    if (d->texturePtr) {
        d->texturePtr->destroy();
    }

    if (d->vao.isCreated()) {
        d->vao.destroy();
    }
}

void StateOverlay::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    Q_UNUSED(fbo)
    Q_UNUSED(metas)

    IMPL(StateOverlay);

    if (d->config->jpegIsRecording()) {
        d->programPtr->bind();
        d->texturePtr->bind();
        d->programPtr->setUniformValue(d->varID, static_cast<int>(d->timer.elapsed() / 2));
        d->functions->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo.fbo);
        d->functions->glEnable(GL_BLEND);
        d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        d->functions->glBlendEquation (GL_FUNC_ADD);
        d->vao.bind();
        d->functions->glDrawElements (GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);
        d->vao.release();
        d->functions->glDisable(GL_BLEND);
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
        d->texturePtr->release();
        d->programPtr->release();
    }

}
