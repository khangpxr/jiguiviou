/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTRECEIVERCONFIG_P_H
#define GVSPQTRECEIVERCONFIG_P_H

#ifdef XMP
#include "gvspqtxmp.h"
#endif

#include "gvspqtdefinitions.h"

#include <QMutex>
#include <QDir>

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

class ReceiverConfigPrivate
{
public:
    QMutex mutex;
    volatile bool isRecording = false;
    QDir path { QDir::homePath() };
    QDir subDir = path;
    volatile bool jpegAutoIncrement = true;
    uint jpegQuality = 85;
#ifdef XMP
    VtoXmp xmp;
#endif

    volatile bool timecodeIsActive = true;
    int timecodeFontHeight = 4;
    TimecodeAlignment timecodeAlignment = TimecodeAlignment::BottomCenter;

    volatile bool reticuleIsActive = true;

    volatile bool blackLevelNeedUpdate = false;
    volatile bool blackLevelIsActive = false;
    volatile bool eqHistogram = false;
    volatile bool drawHistogram = true;
    volatile bool nonHistogramZoneMasked = false;
    int histogramSourceWidth = 100;
    int histogramSourceHeight = 100;

};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQTRECEIVERCONFIG_P_H
